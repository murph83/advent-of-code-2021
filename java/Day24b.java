///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day24b {

    public static void main(String...args) throws Exception{

        //process(Files.readAllLines(Paths.get("../inputs/Day24.input")));

        //System.out.println(new Day24b().valid(List.of(1,3,5,7,9,2,4,6,8,9,9,9,9,9)));

        for(int a = 9; a > 0; a--){
            for(int b = 9; b > 0; b--){
                for(int c = 9; c > 0; c--){
                    for(int d = 9; d > 0; d--){
                        for(int e = 9; e > 0; e--){
                            for(int f = 9; f > 0; f--){
                                for(int g = 9; g > 0; g--){
                                    for(int h = 9; h > 0; h--){
                                        for(int i = 9; i > 0; i--){
                                            for(int j = 9; j > 0; j--){
                                                for(int k = 9; k > 0; k--){
                                                    for(int l = 9; l > 0; l--){
                                                        for(int m = 9; m > 0; m--){
                                                            for(int n = 9; n > 0; n--){
                                                                List<Integer> input = List.of(a, b, c, d, e, f, g, h, i, j, k, l ,m, n);
                                                                String output = "" + a + b + c + +d + e + f + g + h + i + j + k + l + m + n; 
                                                                //System.out.printf("\r%s", output);
                                                                if(new Day24b().valid(input)){
                                                                    System.out.printf("Part 1 result: %s%n", output);
                                                                    System.exit(0);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }    
        }

    }

    int w = 0, x = 0, y = 0, z = 0;

    boolean valid(List<Integer> inputs){
        int inpPtr = 0;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 1;
        x = x + 11;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 14;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 1;
        x = x + 14;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 6;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 1;
        x = x + 15;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 6;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 1;
        x = x + 13;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 13;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 26;
        x = x + -12;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 8;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 1;
        x = x + 10;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 8;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 26;
        x = x + -15;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 7;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 1;
        x = x + 13;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 10;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 1;
        x = x + 10;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 8;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 26;
        x = x + -13;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 12;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 26;
        x = x + -13;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 10;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 26;
        x = x + -14;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 8;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 26;
        x = x + -2;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 8;
        y = y * x;
        z = z + y;
        w = inputs.get(inpPtr++);
        x = x * 0;
        x = x + z;
        x = x % 26;
        z = z / 26;
        x = x + -9;
        x = x == w ? 1 : 0;
        x = x == 0 ? 1 : 0;
        y = y * 0;
        y = y + 25;
        y = y * x;
        y = y + 1;
        z = z * y;
        y = y * 0;
        y = y + w;
        y = y + 7;
        y = y * x;
        z = z + y;

        return z == 0;
    }

    static void process(List<String> instructions){
        System.out.println("int inpPtr = 0;");
        for(String instruction : instructions){
            String[] i = instruction.split(" ");
            switch(i[0]){
                case "inp":
                    System.out.printf("%s = inputs.get(inpPtr++);%n", i[1]);
                    break;
                case "add":
                    System.out.printf("%s = %s + %s;%n", i[1], i[1], i[2]);
                    break;
                case "mul":
                    System.out.printf("%s = %s * %s;%n", i[1], i[1], i[2]);
                    break;
                case "div":
                    System.out.printf("%s = %s / %s;%n", i[1], i[1], i[2]);
                    break;
                case "mod":
                    System.out.printf("%s = %s %% %s;%n", i[1], i[1], i[2]);
                    break;
                case "eql":
                    System.out.printf("%s = %s == %s ? 1 : 0;%n", i[1], i[1], i[2]);
                    break;
            }
        }
    }
    
}
