///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Advent of Code 2021 Day 19
 * <a href="https://adventofcode.com/2021/day/19" target=
 * "_top">https://adventofcode.com/2021/day/19<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day19 {
    static final Path INPUT = Paths.get("../inputs/Day19.input");


    // orientation matrices
    static int[][] o0 = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
    static int[][] o1 = { { 1, 0, 0 }, { 0, 0, 1 }, { 0, -1, 0 } };
    static int[][] o2 = { { 1, 0, 0 }, { 0, -1, 0 }, { 0, 0, -1 } };
    static int[][] o3 = { { 1, 0, 0 }, { 0, 0, -1 }, { 0, 1, 0 } };
    static int[][] o4 = { { -1, 0, 0 }, { 0, 1, 0 }, { 0, 0, -1 } };
    static int[][] o5 = { { -1, 0, 0 }, { 0, 0, 1 }, { 0, 1, 0 } };
    static int[][] o6 = { { -1, 0, 0 }, { 0, -1, 0 }, { 0, 0, 1 } };
    static int[][] o7 = { { -1, 0, 0 }, { 0, 0, -1 }, { 0, -1, 0 } };
    static int[][] o8 = { { 0, 0, -1 }, { 0, 1, 0 }, { 1, 0, 0 } };
    static int[][] o9 = { { 0, 1, 0 }, { 0, 0, 1 }, { 1, 0, 0 } };
    static int[][] o10 = { { 0, 0, 1 }, { 0, -1, 0 }, { 1, 0, 0 } };
    static int[][] o11 = { { 0, -1, 0 }, { 0, 0, -1 }, { 1, 0, 0 } };
    static int[][] o12 = { { 0, 0, 1 }, { 0, 1, 0 }, { -1, 0, 0 } };
    static int[][] o13 = { { 0, -1, 0 }, { 0, 0, 1 }, { -1, 0, 0 } };
    static int[][] o14 = { { 0, 0, -1 }, { 0, -1, 0 }, { -1, 0, 0 } };
    static int[][] o15 = { { 0, 1, 0 }, { 0, 0, -1 }, { -1, 0, 0 } };
    static int[][] o16 = { { 0, 0, -1 }, { 1, 0, 0 }, { 0, -1, 0 } };
    static int[][] o17 = { { 0, 1, 0 }, { 1, 0, 0 }, { 0, 0, -1 } };
    static int[][] o18 = { { 0, 0, 1 }, { 1, 0, 0 }, { 0, 1, 0 } };
    static int[][] o19 = { { 0, -1, 0 }, { 1, 0, 0 }, { 0, 0, 1 } };
    static int[][] o20 = { { 0, 0, 1 }, { -1, 0, 0 }, { 0, -1, 0 } };
    static int[][] o21 = { { 0, -1, 0 }, { -1, 0, 0 }, { 0, 0, -1 } };
    static int[][] o22 = { { 0, 0, -1 }, { -1, 0, 0 }, { 0, 1, 0 } };
    static int[][] o23 = { { 0, 1, 0 }, { -1, 0, 0 }, { 0, 0, 1 } };

    static List<int[][]> orientations = List.of(o0, o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15,
            o16, o17, o18, o19, o20, o21, o22, o23);

    record Vector(int x, int y, int z) {}
    record Point(int x, int y, int z) {}

    static List<Set<Point>> scanners = new ArrayList<>();

    public static void main(String... args) throws Exception{
        List<String> input = Files.readAllLines(INPUT);

        Set<Point> scanner = new HashSet<>();
        for (int i = 0; i < input.size(); i++) {
            if (input.get(i).matches("--- scanner \\d+ ---")) {
                scanner = new HashSet<>();
            } else if (input.get(i).isEmpty()) {
                scanners.add(scanner);
            } else {
                String[] point = input.get(i).split(",");
                scanner.add(
                        new Point(Integer.parseInt(point[0]), Integer.parseInt(point[1]), Integer.parseInt(point[2])));
            }
        }

        Queue<Set<Point>> search = new LinkedList<>(scanners);
        while (search.size() > 1) {
            Set<Point> a = new HashSet<>(search.poll());
            Iterator<Set<Point>> itr = search.iterator();
            while (itr.hasNext()) {
                Set<Point> b = itr.next();
                Optional<Set<Point>> match = orientations.stream()
                        .map(o -> transform(b, o))
                        .map(s -> calculateTranslation(a, s))
                        .flatMap(Optional::stream).findFirst();
                if (match.isPresent()) {
                    a.addAll(match.get());
                    itr.remove();
                }
            }
            search.add(a);
        }

        Set<Point> beacons = search.poll();
        System.out.printf("Part 1 result: %d%n", beacons.size());


        List<Point> scannerLocations = new ArrayList<>();
        scanners.stream().forEach(a -> {
            orientations.stream().map(o -> getTranslation(transform(a, o), beacons))
                .flatMap(Optional::stream).forEach(v -> scannerLocations.add(new Point(v.x, v.y, v.z)));
        });

        int maxDistance = 0;
        for(int i = 0; i < scannerLocations.size(); i++){
            for(int j = 0; j < scannerLocations.size(); j++){
                Point a = scannerLocations.get(i);
                Point b = scannerLocations.get(j);
                maxDistance = Math.max(maxDistance, Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z));
            }
        }

        System.out.printf("Part 2 result: %d%n", maxDistance);
    }

    static Optional<Vector> getTranslation(Set<Point> a, Set<Point> b) {
        // for each pair of points, calculate the translation vector. If there is a
        // vector that shows up more than 12 times, it's probably it.
        Map<Vector, Integer> vectorCount = new HashMap<>();
        for (Point p1 : a) {
            for (Point p2 : b) {
                vectorCount.merge(new Vector(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z), 1, Math::addExact);
            }
        }

        Optional<Vector> translation =  vectorCount.entrySet().stream().filter(e -> e.getValue() >= 12).map(Map.Entry::getKey).findFirst();

        return translation;
    }

    static Optional<Set<Point>> calculateTranslation(Set<Point> a, Set<Point> b) {
        return getTranslation(a, b).map(v -> transform(b, v));
    }

    static Set<Point> transform(Set<Point> a, int[][] transformation) {
        return a.stream()
                .map(p -> new Point(
                        p.x * transformation[0][0] + p.y * transformation[0][1] + p.z * transformation[0][2],
                        p.x * transformation[1][0] + p.y * transformation[1][1] + p.z * transformation[1][2],
                        p.x * transformation[2][0] + p.y * transformation[2][1] + p.z * transformation[2][2]))
                .collect(Collectors.toSet());
    }

    static Set<Point> transform(Set<Point> a, Vector v) {
        return a.stream()
                .map(p -> new Point(p.x - v.x,
                        p.y - v.y,
                        p.z - v.z))
                .collect(Collectors.toSet());
    }

}