///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Supplier;

/**
 * Advent of Code 2021 Day 2
 * <a href="https://adventofcode.com/2021/day/2" target="_top">https://adventofcode.com/2021/day/2<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day02 {
    record Command(String direction, int units) {}

    public static void main(String... args) throws Exception {
        Path input = Paths.get("../inputs/Day02.input");

        final List<Command> commands = Files.readAllLines(input).stream()
                .map(s -> s.split(" "))
                .map(c -> new Command(c[0], Integer.parseInt(c[1])))
                .toList();

        Supplier<Integer> sub1 = () -> {
            int hPos = 0, depth = 0;

            for (Command c : commands) {
                switch (c.direction) {
                    case "forward":
                        hPos += c.units;
                        break;
                    case "down":
                        depth += c.units;
                        break;
                    case "up":
                        depth -= c.units;
                        break;
                }
            }

            return hPos * depth;
        };

        System.out.printf("Part 1 result: %d%n", sub1.get());

        Supplier<Integer> sub2 = () -> {
            int hPos = 0, depth = 0, aim = 0;

            for (Command c : commands) {
                switch (c.direction) {
                    case "forward":
                        hPos += c.units;
                        depth += aim * c.units;
                        break;
                    case "down":
                        aim += c.units;
                        break;
                    case "up":
                        aim -= c.units;
                        break;
                }
            }

            return hPos * depth;
        };

        System.out.printf("Part 2 result: %d%n", sub2.get());
    }
}