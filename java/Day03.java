///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

/**
 * Advent of Code 2021 Day 3
 * <a href="https://adventofcode.com/2021/day/3" target="_top">https://adventofcode.com/2021/day/3<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day03 {

    record ReportEntry(int[] asArray, int asInt){}

    // return an array where the value in each position is the most common bit from the list of input arrays
    static int[] mostCommonBit(List<ReportEntry> input) {
        int[] positions = input.stream()
            .map(ReportEntry::asArray)
            .reduce((left, right) -> IntStream.range(0, left.length).map(i -> left[i] + right[i]).toArray())
            .get();
        Arrays.setAll(positions, x -> (int) Math.round((double)positions[x] / input.size()));

        return positions;
    }
    public static void main(String... args) throws Exception {
        Path input = Paths.get("../inputs/Day03.input");
        final List<ReportEntry> diagnosticReport = Files.readAllLines(input).stream()
            .map(e -> {
                int[] asArray = e.chars().map(x -> x - '0').toArray();
                int asInt = Integer.parseInt(e, 2);
                return new ReportEntry(asArray, asInt);
            }).toList();

        // calculate the gamma rate and epsilon rate
        int[] positions = mostCommonBit(diagnosticReport);

        int gammaRate = IntStream.range(0, positions.length).reduce(0, (n, i)-> (2 * n) + positions[i]);

        int mask = (1 << positions.length) - 1;
        int epsilonRate = ~gammaRate & mask;

        System.out.printf("Part 1 result: %d%n", gammaRate * epsilonRate);


        // find the oxygen rating and the co2 rating
        Function<BiFunction<List<ReportEntry>,Integer,Integer>,Integer> ratingSearch = (criteriaFunction) -> {
            int rating = 0;
            List<ReportEntry> searchSpace = diagnosticReport;
            int entryLength = diagnosticReport.get(0).asArray.length;
            for(int i = 0; i < entryLength; i++){
                int bitSelector = 1 << (entryLength - (i + 1));
                int bitCriteria = criteriaFunction.apply(searchSpace, i); 
    
                List<ReportEntry> matching = searchSpace.stream().filter(entry -> ((entry.asInt & bitSelector) != 0) == (bitCriteria == 1)).toList();
                if(matching.size()  == 1){
                    rating = matching.get(0).asInt;
                    break;
                }
                searchSpace = matching;
            }
            return rating;
        };

        int oxygenRating = ratingSearch.apply((searchSpace, i) -> mostCommonBit(searchSpace)[i]);
        int co2Rating = ratingSearch.apply((searchSpace, i) -> 1 - mostCommonBit(searchSpace)[i]);

        System.out.printf("Part 2 result: %d%n", oxygenRating * co2Rating);
    }
}