///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Advent of Code 2021 Day 18
 * <a href="https://adventofcode.com/2021/day/18" target="_top">https://adventofcode.com/2021/day/18<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day18 {
    static final Path INPUT = Paths.get("../inputs/Day18.input");

    public static void main(String... args) throws Exception {
        Stack<Node> summands = new Stack<>();
        Files.lines(INPUT)
                .flatMapToInt(String::chars)
                .forEach(c -> {
                    if (Character.isDigit((char) c)) {
                        summands.push(new Value(c - '0'));
                    } else if (c == ']') {
                        Node right = summands.pop();
                        Node left = summands.pop();
                        summands.push(new Pair(left, right));
                    }
                });

        System.out.printf("Part 1 result: %d%n", summands.stream().reduce(Day18::add).get().magnitude());

        int max = 0;
        int n = summands.size();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    max = Math.max(max, add(summands.get(i), summands.get(j)).magnitude());
                }

            }
        }

        System.out.printf("Part 2 result: %d%n", max);
    }

    static Pair add(Node left, Node right) {
        return reduce(new Pair(left, right));
    }

    static Pair reduce(Pair p) {
        AtomicBoolean done = new AtomicBoolean();
        do {
            done.set(true);
            List<Value> leaves = p.leaves();

            // explode first node deeper than 4
            OptionalInt ex = IntStream.range(0, leaves.size()).filter(i -> leaves.get(i).depth() > 4).findFirst();
            if (ex.isPresent()) {
                int idx = ex.getAsInt();
                Value left = idx > 0 ? leaves.get(idx - 1) : null;
                Value right = idx < leaves.size() - 2 ? leaves.get(idx + 2) : null;
                leaves.get(idx).parent.explodeInto(left, right);
                done.set(false);
                continue;
            }

            // split first node greater than or equal to 10
            leaves.stream()
                    .filter(node -> node.value >= 10)
                    .findFirst()
                    .ifPresent(node -> {
                        node.split();
                        done.set(false);
                    });
        } while (!done.get());

        return p;
    }

    sealed abstract static class Node permits Pair, Value{
        Pair parent;
        
        long depth(){
            return Stream.iterate(parent, p -> p.parent).takeWhile(Objects::nonNull).count();
        }
        
        abstract Node copy();
        abstract List<Value> leaves();
        abstract int magnitude();
    }

    static final class Value extends Node{
        int value;

        Value(int value){
            this.value = value;
        }

        Value copy(){
            return new Value(this.value);
        }

        List<Value> leaves(){
            return List.of(this);
        }

        int magnitude(){
            return this.value;
        }

        void split() {
            Value left = new Value((int) Math.floor((double) value / 2));
            Value right = new Value((int) Math.ceil((double) value / 2));

            this.parent.replace(this, new Pair(left, right));
        }

        void addTo(Value other){
            if(other != null){
                other.value += this.value;
            }
        }

        @Override
        public String toString() {
            return "" + value;
        }
    }

    static final class Pair extends Node{
        Node left;
        Node right;

        Pair(Node left, Node right) {
            this.left = left.copy();
            this.right = right.copy();
            this.left.parent = this;
            this.right.parent = this;
        }

        public Pair copy() {
            return new Pair(left.copy(), right.copy());
        }

        public List<Value> leaves() {
            return Stream.concat(left.leaves().stream(), right.leaves().stream()).toList();
        }

        public int magnitude() {
            return 3 * left.magnitude() + 2 * right.magnitude();
        }

        void replace(Node original, Node node){
            node.parent = this;
            if(this.left == original) {
                this.left = node;
            } else {
                this.right = node;
            }
        }

        void explodeInto(Value left, Value right) {
            ((Value)this.left).addTo(left);
            ((Value)this.right).addTo(right);
            
            this.parent.replace(this, new Value(0));
        }

        @Override
        public String toString() {
            return "[" + left.toString() + ", " + right.toString() + "]";
        }
    }

}