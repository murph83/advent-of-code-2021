///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Advent of Code 2021 Day 7
 * <a href="https://adventofcode.com/2021/day/7" target="_top">https://adventofcode.com/2021/day/7<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day07 {
    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day07.input");
        int[] positions = Arrays.stream(Files.readString(input).split(",")).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(positions);

        // minimize the linear distance to all points; this is just the median of the set
        int median = positions[positions.length/2];
        System.out.printf("Part 1 result: %s%n", IntStream.of(positions).map(i -> Math.abs(i - median)).sum());

        // fine, we'll implement a search
        long minFuel = IntStream.range(IntStream.of(positions).min().getAsInt(), IntStream.of(positions).max().getAsInt()).mapToLong(d -> fuelCost(positions, d)).min().getAsLong();
        System.out.printf("Part 2 result %s%n", minFuel);
    }

    static long fuelCost(int[] positions, int destination){
        return IntStream.of(positions).asLongStream().map(i -> {
            long x = Math.abs(i - destination);
            return (x * ( x + 1))/2;
        }).sum();
    }
}