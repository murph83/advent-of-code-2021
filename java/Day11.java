///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Advent of Code 2021 Day 11
 * <a href="https://adventofcode.com/2021/day/11" target="_top">https://adventofcode.com/2021/day/11<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day11 {
    static class Octopus {
        boolean flashed = false;
        int startingLevel;
        int level;
        List<Octopus> neighbors = new ArrayList<>();

        Octopus(int level){
            this.startingLevel = level;
            this.level = level;
        }

        void connect(List<Octopus> neighbors){
            this.neighbors = neighbors;
        }

        void step(){
            raiseLevel();
        }

        void reset(){
            if(flashed){
                flashed = false;
                level = 0;
            }
        }

        void backToStart(){
            flashed = false;
            level = startingLevel;
        }

        void raiseLevel(){
            level++;
            if(level > 9 && !flashed){
                flashed = true;
                flashes++;
                for(Octopus o : neighbors){
                    o.raiseLevel();
                }
            }
        }

    }

    static int flashes = 0;

    public static void main(String... args) throws Exception{
        Path input = Paths.get("../inputs/Day11.input");
        List<Octopus> cavern = Files.readAllLines(input).stream()
            .flatMapToInt(line -> line.chars().map(x -> x - '0'))
            .mapToObj(level -> new Octopus(level)).toList();

        int[][] offsets = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};
        IntStream.range(0, 10).forEach(row -> IntStream.range(0, 10).forEach(col -> {
            List<Octopus> neighbors = Arrays.stream(offsets)
                .filter(offset -> (row + offset[0] >= 0) && (row + offset[0] < 10) && (col + offset[1] >= 0) && (col + offset[1] < 10))
                .mapToInt(offset -> (row + offset[0]) * 10 + (col + offset[1]))
                .mapToObj(cavern::get)
                .toList();
            cavern.get(row * 10 + col).connect(neighbors);
        }));

        // count the total flashes over 100 steps
        flashes = 0;
        for(int s = 0; s < 100; s++){
            cavern.stream().forEach(Octopus::step);
            cavern.stream().forEach(Octopus::reset);
        }

        System.out.printf("Part 1 result: %d%n", flashes);

        // find the first step that triggers 100 flashes
        cavern.stream().forEach(Octopus::backToStart);
        flashes = 0;
        int step = 0;
        while(flashes < 100){
            flashes = 0;
            cavern.stream().forEach(Octopus::step);
            cavern.stream().forEach(Octopus::reset);
            step++;
        }

        System.out.printf("Part 2 result: %d%n", step);
    }
}