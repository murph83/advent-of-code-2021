///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Advent of Code 2021 Day 15
 * <a href="https://adventofcode.com/2021/day/15" target="_top">https://adventofcode.com/2021/day/15<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day15 {
    static final Path INPUT = Paths.get("../inputs/Day15.input");

    int[] cave;
    int rows;
    int cols;

    Day15(int[] cave, int rows, int cols){
        this.cave = cave;
        this.rows = rows;
        this.cols = cols;
    }

    public static void main(String... args) throws Exception{
        List<String> lines = Files.readAllLines(INPUT);
        int[] cave = lines.stream().flatMapToInt(line -> line.chars().map(x -> x - '0')).toArray();
        int rows = lines.size();
        int cols = lines.get(0).length();

        Day15 smallCave = new Day15(cave, rows, cols);
        System.out.print("\033c");
        int shortPath = smallCave.aStar(new Node(0,0), new Node(cols - 1, rows - 1), false);

        System.out.printf("Part 1 result [%d x %d]: %d%n", cols, rows, shortPath);

        // make the cave bigger
        int[] cave25 = new int[cave.length * 25];
        int bigRows = rows * 5;
        int bigCols = cols * 5;
        for(int r = 0; r < bigRows; r++){
            for(int c = 0; c < bigCols; c++){
                int level = cave[(r % rows) * cols + (c % cols)] + r / rows + c / cols;
                if(level > 9){
                    level -= 9;
                }
                cave25[(r * bigCols) + c] = level;
            }
        }

        Day15 bigCave = new Day15(cave25, bigRows, bigCols);
        int longPath = bigCave.aStar(new Node(0,0), new Node(bigCols - 1, bigRows - 1), false);

        System.out.printf("Part 2 result [%d x %d]: %d%n", bigCols, bigRows, longPath);
    }

    record Node(int row, int col){}
    Map<Node, Node> parents = new HashMap<>();
    Map<Node, Integer> f = new HashMap<>();
    Map<Node, Integer> g = new HashMap<>();

    int aStar(Node start, Node target, boolean draw) throws InterruptedException{
        PriorityQueue<Node> openList = new PriorityQueue<>(Comparator.comparingInt(f::get));
        Set<Node> closedSet = new HashSet<>();

        g.put(start, 0);
        f.put(start, cols - 1 + rows - 1);
        openList.add(start);

        while(!openList.isEmpty()){
            Node n = openList.poll();
            if(draw) drawWithPath(n);
            if(n.equals(target)){
                return f.get(n);
            }
            closedSet.add(n);
            for(Node m : neighbors(n)){
                if(closedSet.contains(m)){
                    continue;
                }
                int costToEnter = g.get(n) + cave[m.row * cols + m.col];

                if(!openList.contains(m)  || costToEnter < g.get(m)){
                    parents.put(m, n);
                    g.put(m, costToEnter);
                    f.put(m, costToEnter + Math.abs(m.row - target.row) + Math.abs(m.col - target.col));
                    openList.add(m);
                } 
            }

        }
        return -1;
    }

    List<Node> neighbors(Node node){
        List<Node> neighbors = new ArrayList<>();
        if(node.row > 0) neighbors.add(new Node(node.row - 1, node.col));
        if(node.col > 0) neighbors.add(new Node(node.row, node.col - 1));
        if(node.row < rows - 1) neighbors.add(new Node(node.row + 1, node.col));
        if(node.col < cols - 1) neighbors.add(new Node(node.row, node.col + 1));
        return neighbors;
    }

    void draw(){
        drawWithPath(null);
    }

    void drawWithPath(Node end){
        Set<Node> path = Stream.iterate(end, node -> parents.get(node)).takeWhile(node -> node != null).collect(Collectors.toSet());
        StringBuilder map = new StringBuilder();
        map.append("\033[1;1H");
        for(int row = 0; row < rows; row++){
            for(int col = 0; col < cols; col++){
                if(path.contains(new Node(row, col))){
                    map.append("\033[31m");
                }
                map.append(String.format(" %1d", cave[row * cols + col]));
                map.append("\033[m");
            }
            map.append("\n");
        }
        System.out.print(map.toString());
    }
}