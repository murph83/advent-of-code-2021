///usr/bin/env jbang "$0" "$@" ; exit $?

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.IntStream;


/**
 * Advent of Code 2021 Day 23
 * <a href="https://adventofcode.com/2021/day/23" target="_top">https://adventofcode.com/2021/day/23<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day23 {

    enum Amphipod {
        AMBER,
        BRONZE,
        COPPER,
        DESERT,
        EMPTY;
    }

    static Map<Amphipod, Integer> home = Map.of(Amphipod.AMBER,2, Amphipod.BRONZE,4, Amphipod.COPPER,6, Amphipod.DESERT,8);
    static Map<Amphipod, Integer> cost = Map.of(Amphipod.AMBER,1, Amphipod.BRONZE,10, Amphipod.COPPER,100, Amphipod.DESERT,1000);
    record State(List<Amphipod> hallway, Map<Amphipod, List<Amphipod>> rooms) {}

    public static void main(String...args) throws Exception{
        Map<Amphipod, List<Amphipod>> rooms = new EnumMap<>(Amphipod.class);
        rooms.put(Amphipod.AMBER, List.of(Amphipod.BRONZE, Amphipod.DESERT));
        rooms.put(Amphipod.BRONZE, List.of(Amphipod.BRONZE, Amphipod.AMBER));
        rooms.put(Amphipod.COPPER, List.of(Amphipod.COPPER, Amphipod.AMBER));
        rooms.put(Amphipod.DESERT, List.of(Amphipod.DESERT, Amphipod.COPPER));

        State start = new State(new ArrayList<>(Collections.nCopies(11, Amphipod.EMPTY)), rooms);

        Map<Amphipod, List<Amphipod>> target = new EnumMap<>(Amphipod.class);
        target.put(Amphipod.AMBER, List.of(Amphipod.AMBER, Amphipod.AMBER));
        target.put(Amphipod.BRONZE, List.of(Amphipod.BRONZE, Amphipod.BRONZE));
        target.put(Amphipod.COPPER, List.of(Amphipod.COPPER, Amphipod.COPPER));
        target.put(Amphipod.DESERT, List.of(Amphipod.DESERT, Amphipod.DESERT));

        State finish = new State(new ArrayList<>(Collections.nCopies(11, Amphipod.EMPTY)), target);

        System.out.printf("Part 1 result: %s%n", aStar(start, finish));
    }

    static Map<State, Integer> f = new HashMap<>();
    static Map<State, Integer> g = new HashMap<>();

    static int aStar(State start, State target) throws InterruptedException{
        PriorityQueue<State> openList = new PriorityQueue<>(Comparator.comparingInt(f::get));
        Set<State> closedSet = new HashSet<>();

        g.put(start, 0);
        f.put(start, heuristic(start));
        openList.add(start);

        while(!openList.isEmpty()){
            State n = openList.poll();
            if(n.equals(target)){
                return f.get(n);
            }
            closedSet.add(n);
            for(Map.Entry<State, Integer> neighbor : neighbors(n).entrySet()){
                State m = neighbor.getKey();
                int cost = neighbor.getValue();
                if(closedSet.contains(m)){
                    continue;
                }
                int costToEnter = g.get(n) + cost;

                if(!openList.contains(m)  || costToEnter < g.get(m)){
                    g.put(m, costToEnter);
                    f.put(m, costToEnter + heuristic(m));
                    openList.add(m);
                } 
            }

        }
        return -1;
    }

    static int heuristic(State state){
        return 0;
    }

    static IntStream clearPaths(List<Amphipod> hallway, Integer origin) {
        return IntStream.range(0, hallway.size())
                .filter(i -> hallway.subList(Math.min(i, origin), Math.max(i, origin) + 1).stream()
                        .allMatch(a -> Amphipod.EMPTY.equals(a)));
    }

    static Map<State, Integer> neighbors(State state) {
        Map<State, Integer> neighbors = new HashMap<>();

        // if an amphipod is in the hallway, it can only move into its home room with
        // others of its kind
        for (int i = 0; i < 11; i++) {
            Amphipod a = state.hallway.get(i);
            if (a != Amphipod.EMPTY) {
                List<Amphipod> homeRoom = new ArrayList<>(state.rooms.get(a));
                int destination = home.get(a);
                if (clearPaths(state.hallway, i).anyMatch(x -> x == destination) && homeRoom.stream().allMatch(x -> Amphipod.EMPTY.equals(x) || a.equals(x))) {
                    for (int s = homeRoom.size() - 1; s >= 0; s--) {
                        if (Amphipod.EMPTY.equals(homeRoom.get(s))) {
                            homeRoom.set(s, a);
                            List<Amphipod> hallway = new ArrayList<>(state.hallway);
                            hallway.set(i, Amphipod.EMPTY);
                            Map<Amphipod, List<Amphipod>> rooms = new EnumMap<>(Amphipod.class);
                            rooms.putAll(state.rooms);
                            rooms.put(a, homeRoom);
                            int c = (s + 1 + Math.abs(destination - i)) * cost.get(a);
                            neighbors.merge(new State(hallway, rooms), c, Math::max);
                        }
                    }
                }
            }
        }

        // amphipods in incomplete rooms can move to any reachable spot in the hallway
        state.rooms.entrySet().stream()
                .filter(e -> e.getValue().stream().anyMatch(a -> !a.equals(e.getKey())))
                .forEach(e -> {
                    e.getValue().stream()
                            .filter(a -> !Amphipod.EMPTY.equals(a))
                            .findFirst()
                            .ifPresent(a -> {
                                clearPaths(state.hallway, home.get(e.getKey()))
                                .filter(i -> !home.values().contains(i))
                                .forEach(destination ->{
                                    List<Amphipod> room = new ArrayList<>(e.getValue());
                                    int s = room.indexOf(a);
                                    room.set(s, Amphipod.EMPTY);
                                    List<Amphipod> hallway = new ArrayList<>(state.hallway);
                                    hallway.set(destination, a);
                                    Map<Amphipod, List<Amphipod>> rooms = new EnumMap<>(Amphipod.class);
                                    rooms.putAll(state.rooms);
                                    rooms.put(e.getKey(), room);
                                    int c = (Math.abs(destination - home.get(e.getKey())) + s + 1) * cost.get(a);
                                    neighbors.merge(new State(hallway, rooms), c, Math::max);
                                });
                            });
                });

        return neighbors;
    }

}