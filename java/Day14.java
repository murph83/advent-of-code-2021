///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Advent of Code 2021 Day 14
 * <a href="https://adventofcode.com/2021/day/14" target="_top">https://adventofcode.com/2021/day/14<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day14 {
    static final Path INPUT = Paths.get("../inputs/Day14.input");
    static Map<String, String> pairInsertion;
    static Map<String, Map<Character, Long>> frequencyMemo = new HashMap<>();

    public static void main(String... args) throws Exception{
        List<String> inputs = Files.readAllLines(INPUT);

        String template = inputs.get(0);
        pairInsertion = inputs.subList(2, inputs.size()).stream()
            .collect(Collectors
            .toMap((String input) -> {
                       return input.split(" -> ")[0];
                   }, 
                   (String input) -> {
                       return input.split(" -> ")[1];
                   }));


        LongSummaryStatistics tenSteps = frequencies(template, 10).values().stream().mapToLong(Long::longValue).summaryStatistics();
        System.out.printf("Part 1 result: %d%n", tenSteps.getMax() - tenSteps.getMin());

        LongSummaryStatistics fortySteps = frequencies(template, 40).values().stream().mapToLong(Long::longValue).summaryStatistics();
        System.out.printf("Part 1 result: %d%n", fortySteps.getMax() - fortySteps.getMin());

    }

    static Map<Character, Long> frequencies(String elements, int depth){
        if(frequencyMemo.containsKey(elements + depth)){
            return frequencyMemo.get(elements + depth);
        }
        Map<Character, Long> frequencies = new HashMap<>();
        if(depth == 0){
            elements.chars().forEach(c -> frequencies.merge((char)c, 1L, Math::addExact));
        }else{
            String nextStep = step(elements);
            frequencies(nextStep.substring(0, elements.length()), depth - 1).entrySet().stream().forEach(e -> frequencies.merge(e.getKey(), e.getValue(), Math::addExact));
            frequencies(nextStep.substring(elements.length() - 1), depth - 1).entrySet().stream().forEach(e -> frequencies.merge(e.getKey(), e.getValue(), Math::addExact));
            frequencies.merge(nextStep.charAt(elements.length() - 1), 1L, Math::subtractExact);
        }

        frequencyMemo.put(elements + depth, frequencies);
        return frequencies;
    }

    static String step(String polymer){
        char[] chars = polymer.toCharArray();
        StringBuilder s = new StringBuilder();
        for(int i = 0; i < chars.length - 1; i++){
            s.append(chars[i]);
            s.append(pairInsertion.get(new String(new char[]{chars[i], chars[i+1]})));
        }
        s.append(chars[chars.length - 1]);

        return s.toString();
    }
}