///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import java.util.stream.IntStream;

/**
 * Advent of Code 2021 Day 25
 * <a href="https://adventofcode.com/2021/day/25" target="_top">https://adventofcode.com/2021/day/25<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day25 {
    static final Path INPUT = Paths.get("../inputs/Day25.input");

    enum Cucumber{
        EAST,
        SOUTH,
        EMPTY;

        static Cucumber valueOf(int c){
            return switch(c) {
                case 'v' -> Cucumber.SOUTH;
                case '>' -> Cucumber.EAST;
                case '.' -> Cucumber.EMPTY;
                default -> null;
            };
        }
    }

    static List<Cucumber> seaFloor;
    static int w = 0;
    static int h = 0;
    
    
    public static void main(String... args) throws Exception{
        List<String> lines = Files.readAllLines(INPUT);
        h = lines.size();
        w = lines.get(0).length();

        seaFloor = lines.stream()
            .flatMap(line -> line.chars()
                .mapToObj(Cucumber::valueOf))
            .toList();

        seaFloor = new ArrayList<>(seaFloor);

        int steps = 1;
        while(step()){
            steps++;
        }

        System.out.printf("Part 1 result: %s%n", steps);
    }
    
    static boolean step(){
        AtomicBoolean moved = new AtomicBoolean(false);
        // move east
        IntStream.range(0, h).forEach(i -> {
            List<Integer> moving = IntStream.range(0, w)
                .filter(j ->
                    Cucumber.EAST == seaFloor.get(i*w + j) && Cucumber.EMPTY == seaFloor.get(i*w + ((j + 1) % w))).boxed().toList();

            moving.stream()
                .forEach(j -> {
                    Collections.swap(seaFloor, i*w + j, i*w + ((j + 1) % w));
                    moved.set(true);
                });
        });

        // move south
        IntStream.range(0, w).forEach(j -> {
            List<Integer> moving = IntStream.range(0, h)
                .filter(i ->
                    Cucumber.SOUTH == seaFloor.get(i*w + j) && Cucumber.EMPTY == seaFloor.get(((i + 1) % h) * w + j)).boxed().toList();
                
            moving.stream()
                .forEach(i -> {
                    Collections.swap(seaFloor, i*w + j, ((i + 1) % h) * w + j);
                    moved.set(true);
                });
        });

        return moved.get();
    }

    static void display(){
        for(int i = 0; i < h; i++){
            for(int j = 0; j < w; j++){
                char c = switch(seaFloor.get(i*w + j)){
                    case EAST -> '>';
                    case SOUTH -> 'v';
                    case EMPTY -> '.';
                };
                System.out.print(c);
            }
            System.out.print("\n");
        }
    }
}