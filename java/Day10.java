///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Advent of Code 2021 Day 10
 * <a href="https://adventofcode.com/2021/day/10" target="_top">https://adventofcode.com/2021/day/10<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day10 {

    static final Map<Character, Character> parens = Map.of('(',')', '[',']', '{','}', '<','>');
    static final Map<Character, Integer> scoreTable1 = Map.of(')',3, ']',57, '}',1197, '>',25137);
    static final Map<Character, Long> scoreTable2 = Map.of('(',1L, '[',2L, '{',3L, '<',4L);

    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day10.input");
        List<String> lines = Files.readAllLines(input);

        int score1 = 0;
        List<Deque<Character>> incomplete = new ArrayList<>();
        checkCorrupted:
        for(String line : lines){
            Deque<Character> deque = new LinkedList<>();
            for(char c : line.toCharArray()){
                if(parens.keySet().contains(c)){
                    deque.push(c);
                } else if (c != parens.get(deque.pop())){
                    score1 += scoreTable1.get(c);
                    continue checkCorrupted;
                }
            }
            incomplete.add(deque);
        }

        System.out.printf("Part 1 result: %d%n", score1);

        long[] score2 = incomplete.stream()
            .mapToLong(line -> 
                line.stream()
                    .map(scoreTable2::get)
                    .reduce(0L, (total, score) -> 5 * total + score))
            .sorted()
            .toArray();

        System.out.printf("Part 2 result: %d%n", score2[score2.length /2]);

    }
}