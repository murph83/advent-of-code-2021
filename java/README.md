# Advent of Code 2021 - Java Edition
This folder contains Java-based solutions to the AoC 2021 problems.

## Running
### Using JBang
If you have jbang installed, you can simply execute 

```shell
jbang Day01.java
```

or 

```shell 
./Day01.java
``` 
in a supported [shell](https://www.jbang.dev/documentation/guide/latest/usage.html)

### Without JBang
If you want to execute the code without JBang, you can invoke java directly
```shell
java Day01.java --source 11
```

## Solution index (deep link to the code)
|||
|-|-|
| Day  1 |  [Day01.java](Day01.java) |
| Day  2 |  [Day02.java](Day02.java) |
| Day  3 |  [Day03.java](Day03.java) |
| Day  4 |  [Day04.java](Day04.java) |
| Day  5 |  [Day05.java](Day05.java) |
| Day  6 |  [Day06.java](Day06.java) |
| Day  7 |  [Day07.java](Day07.java) |
| Day  8 |  [Day08.java](Day08.java) |
| Day  9 |  [Day08.java](Day09.java) |
| Day 10 |  [Day08.java](Day10.java) |
| Day 11 |  [Day08.java](Day11.java) |
| Day 12 |  [Day08.java](Day12.java) |
| Day 13 |  [Day08.java](Day13.java) |
| Day 14 |  [Day08.java](Day14.java) |
| Day 15 |  [Day08.java](Day15.java) |
| Day 16 |  [Day08.java](Day16.java) |
| Day 17 |  [Day17.java](Day17.java) |
| Day 18 |  [Day17.java](Day18.java) |
| Day 19 |  [Day17.java](Day19.java) |
| Day 20 |  [Day20.java](Day20.java) |
| Day 21 |  [Day20.java](Day21.java) |
