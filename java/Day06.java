///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.LongStream;

/**
 * Advent of Code 2021 Day 6
 * <a href="https://adventofcode.com/2021/day/6" target="_top">https://adventofcode.com/2021/day/6<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day06 {
    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day06.input");

        long[] fish = new long[9];
        try(Scanner scanner = new Scanner(input)){
            scanner.useDelimiter(",");
            while(scanner.hasNextInt()){
                fish[scanner.nextInt()]++;
            }
        }

        System.out.printf("Part 1 result: %d%n", fishAfter(fish, 80));
        System.out.printf("Part 2 result: %d%n", fishAfter(fish, 256));
    
    }

    static long fishAfter(long[] input, long days){
        long[] fish = Arrays.copyOf(input, input.length);

        // it turns out you don't need to rotate the array, just the pointer
        for(int d = 0; d < days; d++){
            fish[(7 + d)%9] += fish[d%9];
        }

        return LongStream.of(fish).sum();
    }

    
}