///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.IntFunction;
import java.util.stream.IntStream;

/**
 * Advent of Code 2021 Day 1
 * <a href="https://adventofcode.com/2021/day/1" target="_top">https://adventofcode.com/2021/day/1<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day01 {
    public static void main(String... args) throws Exception{
        Path input = Paths.get("../inputs/Day01.input");
        
        // parse the input into an array of integers
        final int[] entries = Files.readAllLines(input).stream().mapToInt(Integer::parseInt).toArray();

        // count the number of entries `window` apart that are monotonically increasing
        IntFunction<Long> countIncreasing = (int window) -> IntStream.range(window, entries.length).filter(i -> entries[i] > entries[i - window]).count();

        System.out.printf("Part 1 result: %d%n", countIncreasing.apply(1));
        System.out.printf("Part 2 result: %d%n", countIncreasing.apply(3));
    }
}
