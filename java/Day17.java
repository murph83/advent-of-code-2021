///usr/bin/env jbang "$0" "$@" ; exit $?

/**
 * Advent of Code 2021 Day 17
 * <a href="https://adventofcode.com/2021/day/17" target="_top">https://adventofcode.com/2021/day/17<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day17 {
    static final int maxX = 262, minX = 236, maxY = -58, minY = -78;
    
    public static void main(String... args) throws Exception{

        int onTargetCount = 0;

        for(int vx = 1; vx <= maxX; vx++){
            for(int vy = minY; vy <= -minY; vy++){
                int x = 0, y = 0;
                boolean onTarget = false;
                for(int t = 0; x <= maxX && y >= minY; t++){
                    x += Math.max(0, vx - t);
                    y += (vy - t);
                    onTarget |= x >= minX && y <= maxY && x <= maxX && y >= minY;
                }
                if(onTarget) onTargetCount++;
            }
        }

        System.out.printf("Part 1 result: %d%n", minY * (minY + 1) / 2);
        System.out.printf("Part 2 result: %d%n", onTargetCount);
    }
}