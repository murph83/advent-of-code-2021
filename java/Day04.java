///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Advent of Code 2021 Day 4
 * <a href="https://adventofcode.com/2021/day/4" target="_top">https://adventofcode.com/2021/day/4<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day04 {
    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day04.input");

        List<String> lines = Files.readAllLines(input);
        List<Integer> draws = Stream.of(lines.get(0).split(",")).map(Integer::parseInt).toList();
        List<String> boardInput = lines.subList(2, lines.size());


        // set up the boards
        List<Board> boards = new ArrayList<>();
        for(int i = 0; i < boardInput.size(); i+=6){
            Board board = new Board();
            for(int r = 0; r  < 5; r++){
                String[] cols = boardInput.get(i + r).trim().split("\\s+");
                for(int c = 0; c < 5; c++){
                    board.addPosition(r, c, Integer.parseInt(cols[c]));
                }
            }
            boards.add(board);
        }

        // play until one wins
        part1:
        for(int d : draws){
            for(Board board: boards){
                if(board.mark(d)){
                    System.out.printf("Part 1 result: %d%n", board.score() * d);
                    break part1;
                }
            }        
        }   
        
        // play until the last one wins
        List<Board> winningBoards = new ArrayList<>();
        for(int d : draws){
            for(Board board: boards){
                if(board.mark(d)){
                    winningBoards.add(board);
                }
            }
            boards.removeAll(winningBoards);
        }
        
        Board lastWinner = winningBoards.get(winningBoards.size() - 1);
        System.out.printf("Part 2 result: %d%n", lastWinner.score() * lastWinner.lastMarked);

    }

    static class Board {
        class GridPosition {
            int number;
            boolean marked;

            GridPosition(int number){
                this.number = number;
                this.marked = false;
            }
        }

        GridPosition[] grid = new GridPosition[25];
        int lastMarked;

        void addPosition(int row, int col, int number){
            grid[5*row + col] = new GridPosition(number);
        }

        boolean mark(int number){
            for(GridPosition position : grid){
                if(position.number == number){
                    position.marked = true;
                    lastMarked = number;
                }
            }

            return isBingo();
        }

        boolean isBingo() {
            // rows
            boolean rowMatched = IntStream.range(0, 5).anyMatch(r -> {
                if (IntStream.range(0, 5).mapToObj(c -> grid[5*r + c]).allMatch(p -> p.marked == true)){
                    return true;
                } else {
                    return false;
                }
            });
            

            // cols
            boolean colMatched = IntStream.range(0, 5).anyMatch(c -> {
                if (IntStream.range(0, 5).mapToObj(r -> grid[5*r + c]).allMatch(p -> p.marked == true)){
                    return true;
                } else {
                    return false;
                }
            });

            return rowMatched || colMatched;
        }

        int score(){
            return Stream.of(grid).filter(p -> !p.marked).mapToInt(p -> p.number).sum();
        }
    }
}