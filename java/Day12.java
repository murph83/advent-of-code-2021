///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Advent of Code 2021 Day 12
 * <a href="https://adventofcode.com/2021/day/12" target="_top">https://adventofcode.com/2021/day/12<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
class Day12 {
    static final Path INPUT = Paths.get("../inputs/Day12.input");
    
    public static void main(String... args) throws Exception{
        Map<String, List<String>> caves = Files.lines(INPUT)
            .map(line -> new String[]{line, line.replaceFirst("(\\w+)-(\\w+)", "$2-$1")}) // add the reverse connections as well
            .flatMap(Arrays::stream)
            .collect(Collectors
            .toMap((line) -> line.split("-")[0], 
                   (line) -> List.of(line.split("-")[1]), 
                   (left, right) -> Stream.concat(left.stream(), right.stream()).toList()));

        DFS part1 = new DFS(caves, false);
        part1.search("start", "end");
        System.out.printf("Part 1 result: %d%n", part1.paths.size());

        DFS part2 = new DFS(caves, true);
        part2.search("start", "end");
        System.out.printf("Part 2 result: %d%n", part2.paths.size());
    }
}

class DFS {
    final Map<String, List<String>> caves;
    final boolean allowRevisits;
    List<List<String>> paths = new ArrayList<>();
    Deque<String> currentPath = new LinkedList<>();
    List<String> visited = new ArrayList<>();

    DFS(Map<String, List<String>> caves, boolean allowRevisits){
        this.caves = caves;
        this.allowRevisits = allowRevisits;
    }

    boolean visitedAnyTwice(){
        return visited.stream()
            .collect(Collectors
            .groupingBy(Function.identity(), Collectors.counting()))
            .containsValue(2L);
    }

    void search(String start, String end){
        if(visited.contains(start) && (!allowRevisits || List.of("start", "end").contains(start) || visitedAnyTwice())){
            return;
        }
        
        if(start.chars().allMatch(Character::isLowerCase)){
            visited.add(start);
        }

        currentPath.push(start);
        if(start.equals(end)){
            paths.add(new ArrayList<>(currentPath));
        } else {
            caves.get(start).forEach(neighbor -> search(neighbor, end));
        }

        currentPath.pop();
        visited.remove(start);     
    }
}