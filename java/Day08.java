///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Advent of Code 2021 Day 8
 * <a href="https://adventofcode.com/2021/day/8" target="_top">https://adventofcode.com/2021/day/8<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day08 {
    record PatternsAndDigits(String patterns, List<String> digits) {
    }

    public static void main(String... args) throws Exception {
        // parse input
        Path input = Paths.get("../inputs/Day08.input");
        List<PatternsAndDigits> notes = Files.readAllLines(input).stream()
                .map(note -> note.split("\\|"))
                .map(note -> new PatternsAndDigits(note[0].trim().replaceAll("\\s+", ""), Arrays.asList(note[1].trim().split(" "))))
                .toList();

        // count the number of digits that match size 2, 3, 4, or 7
        long count = notes.stream()
                .map(PatternsAndDigits::digits)
                .flatMap(List::stream)
                .filter(digit -> List.of(2, 3, 4, 7).contains(digit.length()))
                .count();

        System.out.printf("Part 1 result: %d%n", count);

        
        // the sum of global frequencies of each character in the digit, this is invariant
        Map<Integer, Integer> codeToDigits = Map.of(42,0, 17,1, 34,2, 39,3, 30,4, 37,5, 41,6, 25,7, 49,8, 45,9);

        Function<PatternsAndDigits, Integer> decode = (note) -> {
            // get the frequencies in the current pattern
            Map<Character, Integer> frequencies = new HashMap<>();
            note.patterns.chars().forEach(c -> frequencies.merge((char) c, 1, (a, b) -> a + b));

            // decode the digits
            return note.digits.stream()
                    .mapToInt(digit -> digit.chars().map(c -> frequencies.get((char) c)).sum())
                    .map(codeToDigits::get)
                    .reduce(0, (n, d) -> (10 * n) + d);
        };

        int sum = notes.stream()
                .mapToInt(decode::apply)
                .sum();

        System.out.printf("Part 2 result: %d%n", sum);
    }
}