///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Advent of Code 2021 Day 5
 * <a href="https://adventofcode.com/2021/day/5" target="_top">https://adventofcode.com/2021/day/5<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day05 {
    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day05.input");
        List<Line> lines = Files.lines(input).map(Line::new).toList();

        part1(lines);
        part2(lines);
    }

    // count the multiplicities of each gridpoint for vertical and horizontal lines
    static void part1(List<Line> lines){
        Map<Point, Integer> multiplicities = new HashMap<>();
        lines.stream()
            .filter(l -> l.isHorizontal() || l.isVertical())
            .flatMap(l -> l.gridPoints().stream())
            .forEach(p -> multiplicities.merge(p, 1, (v1, v2) -> v1 + v2));

        long overlaps = multiplicities.values().stream().filter(v -> (v >= 2)).count();

        System.out.printf("Part 1 result: %d%n", overlaps);
    }

    // count the multiplicities of each gridpoint for *all* lines
    static void part2(List<Line> lines){
        Map<Point, Integer> multiplicities = new HashMap<>();
        lines.stream()
            .flatMap(l -> l.gridPoints().stream())
            .forEach(p -> multiplicities.merge(p, 1, (v1, v2) -> v1 + v2));

        long overlaps = multiplicities.values().stream().filter(v -> (v >= 2)).count();

        System.out.printf("Part 2 result: %d%n", overlaps);
    }

    record Point(int x, int y){}

    static class Line {
        Point start;
        Point end;

        Line(String input){
            String[] points = input.split("->");
            String[] startPoint = points[0].trim().split(",");
            this.start = new Point(Integer.parseInt(startPoint[0]), Integer.parseInt(startPoint[1]));
    
            String[] endPoint = points[1].trim().split(",");
            this.end = new Point(Integer.parseInt(endPoint[0]), Integer.parseInt(endPoint[1]));
        }

        boolean isVertical(){
            return start.x == end.x;
        }

        boolean isHorizontal(){
            return start.y == end.y;
        }

        boolean isColinear(Point p){
            return (start.x == end.x && end.x == p.x) || ((end.y - start.y)*(p.x - end.x) == (p.y - end.y)*(end.x - start.x));
        }

        // a list of all the grid points that are co-linear with this line
        List<Point> gridPoints(){
            List<Point> gridPoints = new ArrayList<>();
            for(int i = Math.min(start.x, end.x); i <= Math.max(start.x, end.x); i++){
                for(int j = Math.min(start.y, end.y); j <= Math.max(start.y, end.y); j++){
                    Point point = new Point(i,j);
                    if(isColinear(point)){
                        gridPoints.add(point);
                    }
                }
            }

            return gridPoints;
        }
    }
}