///usr/bin/env jbang "$0" "$@" ; exit $?
//JAVAC_OPTIONS --enable-preview --release 17
//JAVA_OPTIONS --enable-preview 

import java.io.CharArrayReader;
import java.io.FilterReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Advent of Code 2021 Day 16
 * <a href="https://adventofcode.com/2021/day/16" target="_top">https://adventofcode.com/2021/day/16<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day16 {
    static final Path INPUT = Paths.get("../inputs/Day16.input");
    static final Map<Character, String> hexdecode = Map.ofEntries(
        Map.entry('0', "0000"),
        Map.entry('1', "0001"),
        Map.entry('2', "0010"),
        Map.entry('3', "0011"),
        Map.entry('4', "0100"),
        Map.entry('5', "0101"),
        Map.entry('6', "0110"),
        Map.entry('7', "0111"),
        Map.entry('8', "1000"),
        Map.entry('9', "1001"),
        Map.entry('A', "1010"),
        Map.entry('B', "1011"),
        Map.entry('C', "1100"),
        Map.entry('D', "1101"),
        Map.entry('E', "1110"),
        Map.entry('F', "1111")
    );

    sealed interface Packet permits LiteralValue, Operator {
        int version();
        int type();
    }
    record LiteralValue(int version, int type, long value) implements Packet{}
    record Operator(int version, int type, List<Packet> subPackets) implements Packet{}
    static List<Packet> packets = new ArrayList<>();

    public static void main(String... args) throws Exception{
        Reader bitstream = new StringReader(Files.readString(INPUT).chars().mapToObj(c -> hexdecode.get((char)c)).collect(Collectors.joining()));
        
        Packet expression = readPacket(bitstream);

        System.out.printf("Part 1 result: %d%n", packets.stream().mapToInt(Packet::version).sum());
        System.out.printf("Part 2 result: %d%n", evaluate(expression));
    }

    static class BinaryReader extends FilterReader{
        BinaryReader(Reader in){
            super(in);
        }

        int readAsInt(int len) throws IOException{
            char[] buffer = new char[len];
            read(buffer, 0, len);
            return Integer.parseInt(String.valueOf(buffer), 2);
        }
    }

    static Packet readPacket(Reader source) throws IOException{
        BinaryReader bitstream = new BinaryReader(source);
        Packet packet = null;
        
        // Every packet begins with a standard header: the first three bits encode the
        // packet version, and the next three bits encode the packet type ID.
        int version = bitstream.readAsInt(3);
        int type = bitstream.readAsInt(3);

        // Packets with type ID 4 represent a literal value. Literal value packets 
        // encode a single binary number. To do this, the binary number is padded with 
        // leading zeroes until its length is a multiple of four bits, and then it is 
        // broken into groups of four bits. Each group is prefixed by a 1 bit except 
        // the last group, which is prefixed by a 0 bit.
        if(type == 4){
            StringBuilder value = new StringBuilder();
            char[] group = new char[4];
            boolean lastgroup = false;
            while(!lastgroup){
                lastgroup = '0' == bitstream.read();
                bitstream.read(group, 0, 4);
                value.append(group);
            }
            packet = new LiteralValue(version, type, Long.parseLong(value.toString(), 2));
        }
        // Every other type of packet (any packet with a type ID other than 4) 
        // represent an operator that performs some calculation on one or more sub-
        // packets contained within. 
        // An operator packet contains one or more packets. To indicate which 
        // subsequent binary data represents its sub-packets, an operator packet can 
        // use one of two modes indicated by the bit immediately after the packet 
        // header; this is called the length type ID:
        //   - If the length type ID is 0, then the next 15 bits are a number that 
        //     represents the total length in bits of the sub-packets contained by 
        //     this packet.
        //   - If the length type ID is 1, then the next 11 bits are a number that 
        //     represents the number of sub-packets immediately contained by this 
        //     packet.
        else {
            List<Packet> subPackets = new ArrayList<>();
            char typeId = (char)bitstream.read();
            if(typeId == '1'){
                int packetCount = bitstream.readAsInt(11);
                for(int i = 0; i < packetCount; i++){
                    subPackets.add(readPacket(bitstream));
                }
            } else {
                int subLength = bitstream.readAsInt(15);
                char[] children = new char[subLength];
                bitstream.read(children, 0, subLength);
                PushbackReader childReader = new PushbackReader(new CharArrayReader(children));
                int c = 0;
                while((c = childReader.read()) > 0){
                    childReader.unread(c);
                    subPackets.add(readPacket(childReader));
                }
            }
            packet = new Operator(version, type, subPackets);
        }

        packets.add(packet);
        return packet;
    }

    static long evaluate(Packet expression){
        return switch(expression){
            case LiteralValue v -> v.value;
            case Operator o -> {
                LongStream subExpressions = o.subPackets.stream().mapToLong(Day16::evaluate);
                yield switch(o.type){
                    case 0 -> subExpressions.sum();
                    case 1 -> subExpressions.reduce(1L, Math::multiplyExact);
                    case 2 -> subExpressions.min().getAsLong();
                    case 3 -> subExpressions.max().getAsLong();
                    case 5 -> subExpressions.reduce((l1, l2) -> l1 > l2 ? 1L : 0L).getAsLong();
                    case 6 -> subExpressions.reduce((l1, l2) -> l1 < l2 ? 1L : 0L).getAsLong();
                    case 7 -> subExpressions.reduce((l1, l2) -> l1 == l2 ? 1L : 0L).getAsLong();
                    default -> throw new RuntimeException();
                };
            }
        };
    }
}