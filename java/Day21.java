///usr/bin/env jbang "$0" "$@" ; exit $?

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Advent of Code 2021 Day 21
 * <a href="https://adventofcode.com/2021/day/21" target="_top">https://adventofcode.com/2021/day/21<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day21 {
    // My Input. Change to 4 and 8 for test
    static final int player1start = 10;
    static final int player2start = 9;
    public static void main(String... args) throws Exception {
        new PracticeDice().play();
        new DiracDice().play();
    }

    static class PracticeDice {
        void play() {

            int[] positions = { player1start - 1, player2start - 1 };
            int[] scores = {0, 0};

            int dice = 1;
            int toss = 0;
            int player = 0;
            while (scores[0] < 1000 && scores[1] < 1000) {
                int roll = 0;
                for (int i = 0; i < 3; i++) {
                    toss++;
                    roll += dice;
                    dice += 1;
                    if (dice == 101)
                        dice = 1;
                }
                int pos = positions[player];
                pos = (pos + roll) % 10;
                positions[player] = pos;

                scores[player] += (pos + 1);
                player = (player + 1) % 2;
            }

            System.out.printf("Part 1 result: %s%n", Math.min(scores[0], scores[1]) * toss);
        }

    }
    
    static class DiracDice{
        // Possible outcomes of dirac dice and their frequency.
        Map<Integer, Integer> rolls = Map.of(3,1, 4,3, 5,6, 6,7, 7,6, 8,3, 9,1);

        void play(){
            int[] position = {player1start - 1, player2start - 1};
            int[] score = {0,0};
            
            long[] wins = nextTurn(0, position, score);
            System.out.printf("Part 2 result: %s%n", Math.max(wins[0], wins[1]));
        }

        long[] nextTurn(int player, int[] position, int[] score){
            long[] wins = {0L, 0L};
            for(int roll : rolls.keySet()){
                int universes = rolls.get(roll);
                int[] nextPosition = Arrays.copyOf(position, 2);
                int[] nextScore = Arrays.copyOf(score, 2);

                nextPosition[player] = (position[player] + roll) % 10;
                nextScore[player] = score[player] + nextPosition[player] + 1;

                if(nextScore[player] >= 21){
                    wins[player] += universes;
                }else{
                    long[] uWins = nextTurn((player + 1) % 2, nextPosition, nextScore);
                    for(int p : List.of(0, 1)){
                        wins[p] += uWins[p] * universes;
                    }
                }
            }
            return wins;
        }
    }

}