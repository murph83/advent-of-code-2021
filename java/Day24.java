///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Advent of Code 2021 Day 24
 * <a href="https://adventofcode.com/2021/day/24" target="_top">https://adventofcode.com/2021/day/24<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day24 {
    static final Path INPUT = Paths.get("../inputs/Day24.input");

    public static void main(String... args) throws Exception{
        List<String> instructions = Files.readAllLines(INPUT);

        ALU alu = new ALU(instructions);

        for(int a = 9; a > 0; a--){
            for(int b = 9; b > 0; b--){
                for(int c = 9; c > 0; c--){
                    for(int d = 9; d > 0; d--){
                        for(int e = 9; e > 0; e--){
                            for(int f = 9; f > 0; f--){
                                for(int g = 9; g > 0; g--){
                                    for(int h = 9; h > 0; h--){
                                        for(int i = 9; i > 0; i--){
                                            for(int j = 9; j > 0; j--){
                                                for(int k = 9; k > 0; k--){
                                                    for(int l = 9; l > 0; l--){
                                                        for(int m = 9; m > 0; m--){
                                                            for(int n = 9; n > 0; n--){
                                                                List<Integer> input = List.of(a, b, c, d, e, f, g, h, i, j, k, l ,m, n);
                                                                String output = "" + a + b + c + +d + e + f + g + h + i + j + k + l + m + n; 
                                                                //System.out.printf("\r%s", output);
                                                                if(alu.process(input)){
                                                                    System.out.printf("Part 1 result: %s%n", output);
                                                                }else{
                                                                    alu.reset();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }    
        }
        
        
    }

}

class ALU{
    List<String> instructions;
    Map<String, Integer> variables = new HashMap<>();

    ALU(List<String> instructions){
        this.instructions = instructions;
        reset();
    }
    
    void reset(){
        variables.put("w", 0);
        variables.put("x", 0);
        variables.put("y", 0);
        variables.put("z", 0);
    }

    boolean process(List<Integer> inputs){
        int inpPtr = 0;
        for(String instruction : instructions){
            try{
            String[] i = instruction.split(" ");
            switch(i[0]){
                case "inp":
                    variables.put(i[1], inputs.get(inpPtr++));
                    break;
                case "add":
                    variables.put(i[1], variables.get(i[1]) + read(i[2]));
                    break;
                case "mul":
                    variables.put(i[1], variables.get(i[1]) * read(i[2]));
                    break;
                case "div":
                    variables.put(i[1], variables.get(i[1]) / read(i[2]));
                    break;
                case "mod":
                    variables.put(i[1], variables.get(i[1]) % read(i[2]));
                    break;
                case "eql":
                    variables.put(i[1], variables.get(i[1]) == read(i[2]) ? 1 : 0);
                    break;
            }
            }catch(Exception e){
                System.out.println("Exception processing instruction: " + instruction);
                System.exit(1);
            }
        }

        return variables.get("z") == 0;
    }

    int read(String variableOrValue){
        if(variableOrValue.matches("-?\\d+")){
            return Integer.parseInt(variableOrValue);
        }else{
            return variables.get(variableOrValue);
        }
    }



}