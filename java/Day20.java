///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Advent of Code 2021 Day 20
 * <a href="https://adventofcode.com/2021/day/20" target="_top">https://adventofcode.com/2021/day/20<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day20 {
    static final Path INPUT = Paths.get("../inputs/Day20.input");

    static int[] enhancementMap;

    public static void main(String... args) throws Exception{
        List<String> input = Files.readAllLines(INPUT);
        
        enhancementMap = input.get(0).chars().map(c -> c == '#' ? 1 : 0).toArray();

        int w = input.get(2).length();
        int h = input.size() - 2;
        int[] i = input.subList(2, input.size()).stream().flatMapToInt(s -> s.chars().map(c -> c == '#' ? 1 : 0)).toArray();

        Image image = new Image(i, w, h);
        for(int n = 0; n < 2; n++){
            int borderChar = n % 2 == 0 ? 0 : 1;
            image.pad(borderChar);
            image.enhance(borderChar);
        }

        System.out.printf("Part 1 result: %s%n", Arrays.stream(image.image).sum());

        for(int n = 2; n < 50; n++){
            int borderChar = n % 2 == 0 ? 0 : 1;
            image.pad(borderChar);
            image.enhance(borderChar);
        }

        System.out.printf("Part 2 result: %s%n", Arrays.stream(image.image).sum());
    }

    static class Image{
        int[] image;
        int w;
        int h;

        Image(int[] image, int w, int h){
            this.image = image;
            this.w = w;
            this.h = h;
        }

        int[][] offsets = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 0}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

        void enhance(int border){
            int[] newi = new int[image.length];
            for(int i = 0; i < image.length; i++){
                int lookup = 0;
                for(int[] offset : offsets){
                    int r = (i / w) + offset[0];
                    int c = (i % w) + offset[1];

                    if(r < 0 || r >= h || c < 0 || c >= w){
                        lookup = (lookup << 1) | border;
                    }else{
                        lookup = (lookup << 1) | image[r*w + c];
                    }
                }
                newi[i] = enhancementMap[lookup];
            }
            image = newi;
        }

        void pad(int pad){
            int neww = w + 2;
            int newh = h + 2;
            int[] newi = new int[neww * newh];

            for(int i = 0; i < neww; i++){
                newi[i] = pad;
            }

            for(int i = 0; i < h; i++){
                int r = i + 1;
                newi[r*neww] = pad;
                for(int j = 0; j < w; j++){
                    int c = j + 1;
                    newi[r*neww + c] = image[i*w + j];
                }
                newi[(r + 1) * neww - 1] = pad;
            }

            for(int i = 0; i < neww; i++){
                newi[(neww * (h + 1)) + i] = pad;
            }

            image = newi;
            h = newh;
            w = neww;
        }

        void print(){
            for(int i = 0; i < h; i++){
                for(int j = 0; j < w; j++){
                    System.out.print(image[i*w + j] == 1 ? "#" : ".");
                }
                System.out.print("\n");
            }
        }
    }

}