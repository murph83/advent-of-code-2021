///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Advent of Code 2021 Day 9
 * <a href="https://adventofcode.com/2021/day/9" target="_top">https://adventofcode.com/2021/day/9<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day09 {
    public static void main(String... args) throws Exception{
        Path input = Paths.get("./Day09.input");

        List<List<Integer>> levels = Files.readAllLines(input).stream().map(row -> row.chars().map(col -> col - '0').boxed().toList()).toList();
        int rows = levels.size();
        int cols = levels.get(0).size();

        final List<Integer> lowPoints = new ArrayList<>();
        int riskLevel = IntStream.range(0, rows).map(row -> {
            return IntStream.range(0, cols).map(col -> {
                List<Integer> neighbors = new ArrayList<>();
                
                for(int r = row - 1; r <= (row + 1); r++){
                    for(int c = col - 1; c <= (col + 1); c++){
                        if(!((c == col) && (r == row)) && !((c < 0) || (r < 0)) && !((c >= cols) || (r >= rows))){
                            neighbors.add(levels.get(r).get(c));
                        }
                    }
                }

                int level = levels.get(row).get(col);
                if(neighbors.stream().allMatch(n -> n > level)){
                    lowPoints.add(level);
                    return level + 1;
                }
                return 0;
            }).sum();
        }).sum();




        System.out.printf("Part 1 result: %d, %s%n", riskLevel, lowPoints.toString());
    }
    
}