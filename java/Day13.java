///usr/bin/env jbang "$0" "$@" ; exit $?

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Advent of Code 2021 Day 13
 * <a href="https://adventofcode.com/2021/day/13" target="_top">https://adventofcode.com/2021/day/13<a>
 * <p>
 * 
 * @author Murph <murph@opusdavei.org>
 */
public class Day13 {
    static final Path INPUT = Paths.get("../inputs/Day13.input");
    record Dot(int x, int y){}
    
    public static void main(String... args) throws Exception{
        List<String> inputs = Files.readAllLines(INPUT);
        int split = inputs.indexOf("");

        Set<Dot> dots = IntStream.range(0, split)
            .mapToObj(i -> {
                String[] xy = inputs.get(i).split(",");
                return new Dot(Integer.parseInt(xy[0]), Integer.parseInt(xy[1]));
            })
            .collect(Collectors.toSet());

        System.out.printf("Part 1 result: %d%n", fold(dots, inputs.get(split + 1)).size());

        for(String foldInstruction : inputs.subList(split + 1, inputs.size())){
            dots = fold(dots, foldInstruction);
        }

        System.out.println("Part 2 result:");
        display(dots);
    }

    static void display(Set<Dot> dots){
        String[][] display = new String[dots.stream().mapToInt(Dot::y).max().getAsInt() + 1][dots.stream().mapToInt(Dot::x).max().getAsInt() + 1];
        for(String[] row : display){
            Arrays.fill(row, " ");
        }
        for(Dot dot : dots){
            display[dot.y][dot.x] = "#";
        }

        for(String[] row : display){
            System.out.println(String.join("", row));
        }
    }

    static Set<Dot> fold(Set<Dot> dots, String foldInstruction){
        String[] fold = foldInstruction.replaceAll("fold along (x|y)=(\\d+)", "$1,$2").split(",");
        int foldAxis = Integer.parseInt(fold[1]);
        return dots.stream()
            .map(dot ->{
                if("x".equals(fold[0])){
                    if(dot.x < foldAxis){
                        return dot;
                    }else{
                        return new Dot(foldAxis - (dot.x - foldAxis), dot.y);
                    }
                }else{
                    if(dot.y < foldAxis){
                        return dot;
                    }else{
                        return new Dot(dot.x, foldAxis - (dot.y - foldAxis));
                    }
                }
            })
            .collect(Collectors.toSet());
    }
}