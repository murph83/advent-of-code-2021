package main

import (
	"bufio"
	"fmt"
	"os"
)

type point struct {
	x, y int
}

var grid = make(map[point]int)

func main() {
	readFile, _ := os.Open("../inputs/Day11.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	i := 0
	for fileScanner.Scan() {
		for j, r := range fileScanner.Text() {
			grid[point{i, j}] = int(r - '0')
		}
		i++
	}

	part1()
	part2()
}

func part1() {
	flashes := 0
	for i := 0; i < 100; i++ {
		flashes += step()
	}

	fmt.Printf("Part 1: %d\n", flashes)
}

func part2() {
	steps := 101
	for step() < 100 {
		steps++
	}

	fmt.Printf("Part 2: %d\n", steps)
}

func step() int {
	flashed := make(map[point]struct{})

	for p := range grid {
		grid[p]++
	}

	dirty := true
	for dirty {
		dirty = false
		for p := range grid {
			if _, f := flashed[p]; !f && grid[p] > 9 {
				flashed[p] = struct{}{}
				for i := -1; i <= 1; i++ {
					for j := -1; j <= 1; j++ {
						if p.x+i >= 0 && p.x+i < 10 && p.y+j >= 0 && p.y+j < 10 {
							grid[point{p.x + i, p.y + j}]++
							dirty = true
						}
					}
				}
			}
		}
	}

	for p := range grid {
		if grid[p] > 9 {
			grid[p] = 0
		}
	}

	return len(flashed)
}
