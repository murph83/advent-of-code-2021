package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

type point struct {
	x, y int
}

var points = make(map[point]int)
var xmax, ymax int
var lowpoints []point

func main() {
	readFile, _ := os.Open("../inputs/Day09.input")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	i := 0
	for fileScanner.Scan() {
		for j, r := range fileScanner.Text() {
			points[point{i, j}] = int(r - '0')
		}
		i++
	}
	xmax = i
	ymax = len(points) / xmax

	part1()
	part2()
}

func part1() {
	risklevel := 0
	for p := range points {
		if p.isLowPoint() {
			lowpoints = append(lowpoints, p)
			risklevel += (points[p] + 1)
		}
	}

	fmt.Printf("Part 1: %d\n", risklevel)
}

func part2() {
	var basins []int
	for _, lp := range lowpoints {
		basins = append(basins, basinSize(lp)+1)
	}

	sort.Ints(basins)

	fmt.Printf("Part 2: %d\n", basins[len(basins)-1]*basins[len(basins)-2]*basins[len(basins)-3])
}

func (p *point) isLowPoint() bool {
	return (p.x == 0 || points[point{p.x - 1, p.y}] > points[*p]) &&
		(p.y == 0 || points[point{p.x, p.y - 1}] > points[*p]) &&
		(p.x == xmax-1 || points[point{p.x + 1, p.y}] > points[*p]) &&
		(p.y == ymax-1 || points[point{p.x, p.y + 1}] > points[*p])
}

func basinSize(lowpoint point) int {
	basin := make(map[point]struct{})
	search := []point{lowpoint}
	for len(search) > 0 {
		var next []point
		for _, p := range search {
			for _, n := range p.neighbors() {
				next = append(next, n)
				basin[n] = struct{}{}
			}

		}
		search = next
	}

	return len(basin)
}

func (p *point) neighbors() []point {
	var n []point
	left := point{p.x - 1, p.y}
	up := point{p.x, p.y - 1}
	right := point{p.x + 1, p.y}
	down := point{p.x, p.y + 1}

	if p.x > 0 && points[left] < 9 && points[left] > points[*p] {
		n = append(n, left)
	}
	if p.y > 0 && points[up] < 9 && points[up] > points[*p] {
		n = append(n, up)
	}
	if p.x < xmax && points[right] < 9 && points[right] > points[*p] {
		n = append(n, right)
	}
	if p.y < ymax && points[down] < 9 && points[down] > points[*p] {
		n = append(n, down)
	}

	return n
}
