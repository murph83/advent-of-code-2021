package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var commands = []command{}
var depth = 0
var position = 0
var aim = 0

type command struct {
	direction string
	units     int
}

func main() {
	readFile, _ := os.Open("../inputs/Day02.input")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		c := strings.Fields(fileScanner.Text())
		i, _ := strconv.Atoi(c[1])
		commands = append(commands, command{c[0], i})
	}

	part1()
	part2()
}

func part1() {
	for _, c := range commands {
		switch c.direction {
		case "forward":
			position += c.units
		case "up":
			depth -= c.units
		case "down":
			depth += c.units
		}
	}

	fmt.Printf("Part 1: %d\n", depth*position)
}

func part2() {
	depth = 0
	position = 0
	aim = 0
	for _, c := range commands {
		switch c.direction {
		case "forward":
			position += c.units
			depth += aim * c.units
		case "up":
			aim -= c.units
		case "down":
			aim += c.units
		}
	}

	fmt.Printf("Part 2: %d\n", depth*position)
}
