package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var report [][]int

func main() {
	readFile, _ := os.Open("../inputs/Day03.input")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		digits := make([]int, len(line))
		for i, d := range line {
			digits[i] = int(d - '0')
		}
		report = append(report, digits)
	}

	part1()
	part2()

}

func part1() {
	gammaRateBits := make([]int, len(report[0]))
	epsilonRateBits := make([]int, len(report[0]))

	for i := 0; i < len(report[0]); i++ {
		column := getColumn(report, i)

		gammaRateBits[i] = mostCommonBit(column)
		epsilonRateBits[i] = leastCommonBit(column)
	}

	gammaRate := bits2Int(gammaRateBits)
	epsilonRate := bits2Int(epsilonRateBits)

	fmt.Printf("Part 1: %d\n", gammaRate*epsilonRate)
}

func part2() {
	oxygenRate := bits2Int(search(mostCommonBit, report[:]))
	co2Rate := bits2Int(search(leastCommonBit, report[:]))

	fmt.Printf("Part 2: %d\n", co2Rate*oxygenRate)
}

func bits2Int(bits []int) int {
	rateString := make([]string, len(bits))
	for i, b := range bits {
		rateString[i] = strconv.Itoa(b)
	}

	rate, _ := strconv.ParseInt(strings.Join(rateString, ""), 2, 0)

	return int(rate)
}

func getColumn(space [][]int, col int) []int {
	column := make([]int, len(space))
	for r, c := range space {
		column[r] = c[col]
	}
	return column
}

func search(f func([]int) int, space [][]int) []int {
	searchSpace := space
	i := 0

	for len(searchSpace) > 1 {
		var newSearch [][]int
		column := getColumn(searchSpace, i)
		bitCriteria := f(column)

		for _, row := range searchSpace {
			if row[i] == bitCriteria {
				newSearch = append(newSearch, row)
			}
		}

		searchSpace = newSearch
		i++
	}

	return searchSpace[0]
}

func mostCommonBit(bits []int) int {
	var sum float64 = 0
	for _, b := range bits {
		sum += float64(b)
	}

	var threshold float64 = float64(len(bits)) / float64(2)

	if sum >= threshold {
		return 1
	} else {
		return 0
	}
}

func leastCommonBit(bits []int) int {
	var sum float64 = 0
	for _, b := range bits {
		sum += float64(b)
	}

	var threshold float64 = float64(len(bits)) / float64(2)

	if sum >= threshold {
		return 0
	} else {
		return 1
	}
}
