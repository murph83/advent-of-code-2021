package main

import (
	"bufio"
	"fmt"
	"os"
)

type line struct {
	signalpattern []string
	output        []string
}

var lines []line

func main() {
	readFile, _ := os.Open("../inputs/Day08.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		var p0, p1, p2, p3, p4, p5, p6, p7, p8, p9 string
		var o0, o1, o2, o3 string

		fmt.Sscanf(fileScanner.Text(), "%s %s %s %s %s %s %s %s %s %s | %s %s %s %s", &p0, &p1, &p2, &p3, &p4, &p5, &p6, &p7, &p8, &p9, &o0, &o1, &o2, &o3)

		signalpattern := []string{p0, p1, p2, p3, p4, p5, p6, p7, p8, p9}
		output := []string{o0, o1, o2, o3}
		lines = append(lines, line{signalpattern, output})
	}

	part1()
	part2()
}

func part1() {
	uniq := 0
	for _, l := range lines {
		for _, o := range l.output {
			switch len(o) {
			case 2, 3, 4, 7:
				uniq++
			}
		}
	}

	fmt.Printf("Part 1: %d\n", uniq)
}

func part2() {
	// the sum of global frequencies of each character in the digit, this is invariant
	codeToDigits := map[int]int{
		42: 0,
		17: 1,
		34: 2,
		39: 3,
		30: 4,
		37: 5,
		41: 6,
		25: 7,
		49: 8,
		45: 9,
	}

	sum := 0
	for _, l := range lines {
		signalFrequencies := make(map[rune]int, 7)
		for _, sp := range l.signalpattern {
			for _, r := range sp {
				signalFrequencies[r]++
			}
		}

		outputValue := 0
		for _, o := range l.output {
			code := 0
			for _, r := range o {
				code += signalFrequencies[r]
			}
			outputValue = (10 * outputValue) + codeToDigits[code]
		}

		sum += outputValue
	}

	fmt.Printf("Part 2: %d\n", sum)
}
