package main

import (
	"bufio"
	"fmt"
	"os"
)

var offsets = [][]int{
	{-1, -1},
	{-1, 0},
	{-1, 1},
	{0, -1},
	{0, 0},
	{0, 1},
	{1, -1},
	{1, 0},
	{1, 1},
}

type pixel int

const (
	Light pixel = 1
	Dark  pixel = 0
)

var imageEnhancement = make([]pixel, 512)

type image struct {
	height, width int
	pixels        []pixel
	border        pixel
}

var currentImage image

func main() {
	input, err := os.Open("../inputs/Day20.input")
	//input, err := os.Open("./test")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer input.Close()

	fileScanner := bufio.NewScanner(input)
	fileScanner.Split(bufio.ScanLines)

	fileScanner.Scan()

	for i, r := range fileScanner.Text() {
		switch r {
		case '#':
			imageEnhancement[i] = Light
		case '.':
			imageEnhancement[i] = Dark
		}

	}

	fileScanner.Scan()

	var pixels []pixel
	var h int
	for ; fileScanner.Scan(); h++ {
		for _, p := range fileScanner.Text() {
			switch p {
			case '#':
				pixels = append(pixels, Light)
			case '.':
				pixels = append(pixels, Dark)

			}
		}
	}
	currentImage = image{h, len(pixels) / h, pixels, Dark}

	part1()
	part2()
}

func part1() {
	currentImage.enhance()
	currentImage.enhance()

	sum := 0
	for _, p := range currentImage.pixels {
		sum += int(p)
	}

	fmt.Printf("Part 1: %d\n", sum)
}

func part2() {
	for i := 2; i < 50; i++ {
		currentImage.enhance()
	}

	sum := 0
	for _, p := range currentImage.pixels {
		sum += int(p)
	}

	fmt.Printf("Part 2: %d\n", sum)
}

func (img *image) enhance() {
	newHeight := img.height + 2
	newWidth := img.width + 2
	pixels := make([]pixel, newHeight*newWidth)
	for i := -1; i < img.height+1; i++ {
		for j := -1; j < img.width+1; j++ {
			eIdx := 0
			for _, o := range offsets {
				eIdx = eIdx * 2
				offseti, offsetj := i+o[0], j+o[1]
				if offseti >= 0 && offseti < img.height &&
					offsetj >= 0 && offsetj < img.width {

					eIdx += int(img.pixels[offseti*img.width+offsetj])
				} else {
					eIdx += int(img.border)
				}
			}
			pixels[(i+1)*newWidth+(j+1)] = imageEnhancement[eIdx]
		}
	}

	img.height = newHeight
	img.width = newWidth
	img.pixels = pixels
	switch img.border {
	case Light:
		img.border = imageEnhancement[len(imageEnhancement)-1]
	case Dark:
		img.border = imageEnhancement[0]
	}
}
