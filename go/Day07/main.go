package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

var crabs []int

func main() {
	readFile, _ := os.Open("../inputs/Day07.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	fileScanner.Scan()
	for _, s := range strings.Split(fileScanner.Text(), ",") {
		c, _ := strconv.Atoi(s)
		crabs = append(crabs, c)
	}
	sort.Ints(crabs)

	part1()
	part2()
}

func part1() {
	median := crabs[len(crabs)/2]
	totalFuel := 0
	for _, c := range crabs {
		fuelCost := median - c
		if fuelCost < 0 {
			fuelCost = -fuelCost
		}

		totalFuel += fuelCost
	}

	fmt.Printf("Part 1: %d\n", totalFuel)
}

func part2() {
	minFuel := math.MaxInt

	// for each posible position, check the fuel cost
	for x := crabs[0]; x < crabs[len(crabs)-1]; x++ {
		fuel := 0
		for _, c := range crabs {
			d := c - x
			if d < 0 {
				d = -d
			}
			fuel += (d * (d + 1)) / 2
		}

		if fuel < minFuel {
			minFuel = fuel
		}
	}

	fmt.Printf("Part 2: %d\n", minFuel)
}
