package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
)

type number struct {
	value      int
	prev, next *number
	depth      int
}

var numbers []*number

func main() {
	readFile, _ := os.Open("../inputs/Day18.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		var h, p *number
		depth := 0
		for _, r := range fileScanner.Text() {
			if r == '[' {
				depth++
			} else if r == ']' {
				depth--
			} else if unicode.IsDigit(r) {
				num := &number{
					value: int(r - '0'),
					depth: depth,
					prev:  p,
					next:  nil,
				}
				if h == nil {
					h = num
					p = num
				} else {
					p.next = num
					p = num
				}
			}
		}

		numbers = append(numbers, h)
	}

	part1()
	part2()
}

func part1() {
	sum := add(numbers[0], numbers[1])

	for i := 2; i < len(numbers); i++ {
		sum = add(sum, numbers[i])
	}

	fmt.Printf("Part 1: %d\n", sum.magnitude())
}

func part2() {
	max := 0
	for i := 0; i < len(numbers); i++ {
		for j := 0; j < len(numbers); j++ {
			if j != i {
				if sum := add(numbers[i], numbers[j]).magnitude(); sum > max {
					max = sum
				}
			}
		}
	}

	fmt.Printf("Part 2: %d\n", max)
}

func add(left *number, right *number) *number {
	// copy the head of the left number
	h := *left
	h.depth++

	// make it the head of the new number
	var n, curr *number
	n = &h
	curr = &h

	for p := left.next; p != nil; p = p.next {
		num := *p
		num.depth++
		curr.next = &num
		num.prev = curr
		curr = &num
	}

	for p := right; p != nil; p = p.next {
		num := *p
		num.depth++
		curr.next = &num
		num.prev = curr
		curr = curr.next
	}

	return reduce(n)
}

func (n *number) magnitude() int {
	for depth := 4; depth > 0; depth-- {
		for p := n; p != nil; p = p.next {
			if p.depth == depth && p.next != nil && p.next.depth == depth {
				new := &number{
					value: 3*p.value + 2*p.next.value,
					depth: p.depth - 1,
					prev:  p.prev,
					next:  p.next.next,
				}
				if p.prev != nil {
					p.prev.next = new
				} else {
					n = new
				}
				if p.next.next != nil {
					p.next.next.prev = new
				}
			}
		}
	}

	return n.value
}

func reduce(n *number) *number {
	for p := n; p != nil; p = p.next {
		if p.depth > 4 {
			new := &number{
				value: 0,
				depth: p.depth - 1,
				prev:  p.prev,
				next:  p.next.next,
			}
			if p.prev != nil {
				p.prev.value += p.value
				p.prev.next = new
			}
			if p.next.next != nil {
				p.next.next.value += p.next.value
				p.next.next.prev = new
			}

			if p == n {
				return reduce(new)
			} else {
				return reduce(n)
			}
		}
	}

	for p := n; p != nil; p = p.next {
		if p.value >= 10 {
			newLeft := &number{
				value: p.value / 2,
				depth: p.depth + 1,
				prev:  p.prev,
				next:  nil,
			}
			newRight := &number{
				value: p.value - (p.value / 2),
				depth: p.depth + 1,
				prev:  newLeft,
				next:  p.next,
			}
			newLeft.next = newRight

			if p.prev != nil {
				p.prev.next = newLeft
			}
			if p.next != nil {
				p.next.prev = newRight
			}

			if p == n {
				return reduce(newLeft)
			} else {
				return reduce(n)
			}
		}
	}

	return n
}

func (n *number) String() string {
	var sb strings.Builder

	for p := n; p != nil; p = p.next {
		sb.WriteString(fmt.Sprintf("v: %d, depth: %d -> ", p.value, p.depth))
	}

	return sb.String()
}
