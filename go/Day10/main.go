package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

var pairs = map[rune]rune{
	')': '(',
	']': '[',
	'}': '{',
	'>': '<',
}

var lines []string
var incomplete []string

func main() {
	readFile, _ := os.Open("../inputs/Day10.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		lines = append(lines, fileScanner.Text())
	}

	part1()
	part2()
}

func part1() {
	var scores = map[rune]int{
		')': 3,
		']': 57,
		'}': 1197,
		'>': 25137,
	}

	score := 0

corruptioncheck:
	for _, l := range lines {
		var stack []rune
		for _, r := range l {
			if r == ')' || r == ']' || r == '}' || r == '>' {
				if pairs[r] == stack[len(stack)-1] {
					stack = stack[:len(stack)-1]
				} else {
					score += scores[r]
					continue corruptioncheck
				}
			} else {
				stack = append(stack, r)
			}

		}
		incomplete = append(incomplete, l)
	}

	fmt.Printf("Part 1: %d\n", score)
}

func part2() {
	var scores = map[rune]int{
		'(': 1,
		'[': 2,
		'{': 3,
		'<': 4,
	}

	stringscores := make([]int, len(incomplete))
	for i, l := range incomplete {
		var stack []rune
		for _, r := range l {
			if r == ')' || r == ']' || r == '}' || r == '>' {
				stack = stack[:len(stack)-1]
			} else {
				stack = append(stack, r)
			}
		}

		for j := len(stack) - 1; j >= 0; j-- {
			stringscores[i] = stringscores[i]*5 + scores[stack[j]]
		}
	}

	sort.Ints(stringscores)

	fmt.Printf("Part 2: %d\n", stringscores[len(stringscores)/2])
}
