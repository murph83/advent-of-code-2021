package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

var input = []int{}

func main() {

	readFile, _ := os.Open("../inputs/Day01.input")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		n, _ := strconv.Atoi(fileScanner.Text())
		input = append(input, n)
	}

	part1()
	part2()
}

func part1() {
	count := 0

	for i, n := range input {
		if i > 0 && n > input[i-1] {
			count++
		}
	}

	fmt.Printf("Part 1: %d\n", count)
}

func part2() {
	count := 0
	previous := math.MaxInt

	for i := 2; i < len(input); i++ {
		sum := input[i] + input[i-1] + input[i-2]
		if sum > previous {
			count++
		}
		previous = sum
	}

	fmt.Printf("Part 2: %d\n", count)
}
