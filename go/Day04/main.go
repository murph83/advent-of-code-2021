package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type board struct {
	numbers []int
	marked  []bool
	last    int
}

var lines []string

func main() {
	readFile, _ := os.Open("../inputs/Day04.input")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		lines = append(lines, fileScanner.Text())
	}

	part1()
	part2()

}

func part1() {
	calls, boards := parseinput()
	for _, n := range calls {
		for _, b := range boards {
			if b.mark(n) {
				fmt.Printf("Part 1: %d\n", b.score())
				return
			}
		}
	}
}

func part2() {
	calls, boards := parseinput()
	var winning []board
	for _, n := range calls {
		for _, b := range boards {
			if b.mark(n) {
				winning = append(winning, b)
			}
		}
	}

	fmt.Printf("Part 2: %d\n", winning[len(winning)-1].score())
}

func (b *board) mark(n int) bool {
	if b.isBingo() {
		return false
	}

	if i := indexOf(b.numbers, n); i >= 0 {
		b.marked[i] = true
		b.last = n

		return b.isBingo()
	} else {
		return false
	}
}

func (b *board) isBingo() bool {
	rowfull := (b.marked[0] && b.marked[1] && b.marked[2] && b.marked[3] && b.marked[4]) || (b.marked[5] && b.marked[6] && b.marked[7] && b.marked[8] && b.marked[9]) || (b.marked[10] && b.marked[11] && b.marked[12] && b.marked[13] && b.marked[14]) || (b.marked[15] && b.marked[16] && b.marked[17] && b.marked[18] && b.marked[19]) || (b.marked[20] && b.marked[21] && b.marked[22] && b.marked[23] && b.marked[24])
	colfull := (b.marked[0] && b.marked[5] && b.marked[10] && b.marked[15] && b.marked[20]) || (b.marked[1] && b.marked[6] && b.marked[11] && b.marked[16] && b.marked[21]) || (b.marked[2] && b.marked[7] && b.marked[12] && b.marked[17] && b.marked[22]) || (b.marked[3] && b.marked[8] && b.marked[13] && b.marked[18] && b.marked[23]) || (b.marked[4] && b.marked[9] && b.marked[14] && b.marked[19] && b.marked[24])

	return rowfull || colfull
}

func (b *board) score() int {
	sum := 0
	for i, a := range b.numbers {
		if !b.marked[i] {
			sum += a
		}
	}
	return sum * b.last
}

func indexOf(s []int, n int) int {
	for i, v := range s {
		if v == n {
			return i
		}
	}
	return -1
}

func parseinput() ([]int, []board) {
	var boards []board
	var calls []int

	for _, s := range strings.Split(lines[0], ",") {
		call, _ := strconv.Atoi(s)
		calls = append(calls, call)
	}

	for i := 2; i < len(lines); i += 6 {
		n := make([]int, 25)
		for j := 0; j < 5; j++ {
			s := strings.Fields(lines[i+j])
			for x := range s {
				n[5*j+x], _ = strconv.Atoi(s[x])
			}
		}

		boards = append(boards, board{n, make([]bool, 25), -1})
	}

	return calls, boards
}
