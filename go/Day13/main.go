package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type point struct {
	x, y int
}

type fold struct {
	axis   string
	offset int
}

var dots = make(map[point]struct{})
var folds []fold

func main() {
	readFile, _ := os.Open("../inputs/Day13.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	p := make([]int, 2)
	readingdots := true
	for readingdots && fileScanner.Scan() {
		l := fileScanner.Text()
		if l == "" {
			readingdots = false
			continue
		}
		for i, s := range strings.Split(l, ",") {
			p[i], _ = strconv.Atoi(s)
		}
		dots[point{p[0], p[1]}] = struct{}{}
	}

	var (
		line, axis string
		offset     int
	)

	for fileScanner.Scan() {
		fmt.Sscanf(fileScanner.Text(), "fold along %s", &line)
		f := strings.Split(line, "=")
		axis = f[0]
		offset, _ = strconv.Atoi(f[1])

		folds = append(folds, fold{axis, offset})
	}

	part1()
	part2()
}

func part1() {
	doFold(folds[0])

	fmt.Printf("Part 1: %d\n", len(dots))
}

func part2() {
	for n := 1; n < len(folds); n++ {
		doFold(folds[n])
	}

	fmt.Println("Part 2:")
	render()
}

func render() {
	xmax := 0
	ymax := 0
	for p := range dots {
		if p.x > xmax {
			xmax = p.x
		}
		if p.y > ymax {
			ymax = p.y
		}
	}

	paper := make([][]string, ymax+1)
	for y := 0; y <= ymax; y++ {
		row := make([]string, xmax+1)
		for x := 0; x <= xmax; x++ {
			if _, d := dots[point{x, y}]; d {
				row[x] = "#"
			} else {
				row[x] = " "
			}
		}
		paper[y] = row
	}

	for y := 0; y <= ymax; y++ {
		fmt.Println(paper[y])
	}
}

func doFold(f fold) {
	for p := range dots {
		if f.axis == "y" && p.y > f.offset {
			dots[point{p.x, f.offset - (p.y - f.offset)}] = struct{}{}
			delete(dots, p)
		}
		if f.axis == "x" && p.x > f.offset {
			dots[point{f.offset - (p.x - f.offset), p.y}] = struct{}{}
			delete(dots, p)
		}

	}
}
