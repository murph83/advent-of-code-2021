package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var fish = make([]int, 9)

func main() {
	readFile, _ := os.Open("../inputs/Day06.input")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	fileScanner.Scan()
	for _, s := range strings.Split(fileScanner.Text(), ",") {
		f, _ := strconv.Atoi(s)
		fish[f]++
	}

	for i := 0; i < 256; i++ {
		fish[(7+i)%9] += fish[i%9] // fish magic
		if i == 79 {
			fmt.Printf("Part 1: %d\n", sum())
		}
	}

	fmt.Printf("Part 2: %d\n", sum())
}

func sum() int {
	sum := 0
	for _, f := range fish {
		sum += f
	}

	return sum
}
