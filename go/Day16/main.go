package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

var transmission Packet

type header struct {
	version, typeId int
}

type literal struct {
	header
	value int
}

type operator struct {
	header
	subpackets []Packet
}

type Packet interface {
	Checksum() int
	Evaluate() int
}

var hexcode = map[string]string{
	"0": "0000",
	"1": "0001",
	"2": "0010",
	"3": "0011",
	"4": "0100",
	"5": "0101",
	"6": "0110",
	"7": "0111",
	"8": "1000",
	"9": "1001",
	"A": "1010",
	"B": "1011",
	"C": "1100",
	"D": "1101",
	"E": "1110",
	"F": "1111",
}

func main() {
	readFile, _ := os.Open("../inputs/Day16.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanRunes)

	var sb strings.Builder
	for fileScanner.Scan() {
		sb.WriteString(hexcode[fileScanner.Text()])
	}
	transmission = readPacket(strings.NewReader(sb.String()))

	part1()
	part2()
}

func part1() {

	fmt.Printf("Part 1: %d\n", transmission.Checksum())

}

func part2() {

	fmt.Printf("Part 2: %d\n", transmission.Evaluate())

}

func readInt(reader *strings.Reader, len int) int {
	b := make([]byte, len)
	reader.Read(b)

	val, _ := strconv.ParseInt(string(b), 2, 64)
	return int(val)
}

func readPacket(reader *strings.Reader) Packet {
	version := readInt(reader, 3)
	typeId := readInt(reader, 3)

	if typeId == 4 {
		b := make([]byte, 4)
		var sb strings.Builder
		lastgroup := false
		for !lastgroup {
			r, _, _ := reader.ReadRune()
			lastgroup = r == '0'
			reader.Read(b)
			sb.WriteString(string(b))
		}

		value, _ := strconv.ParseInt(sb.String(), 2, 64)
		return Packet(literal{header{version, typeId}, int(value)})
	} else {
		var subpackets []Packet
		r, _, _ := reader.ReadRune()
		if r == '0' {
			subpacketLength := readInt(reader, 15)
			b := make([]byte, subpacketLength)
			reader.Read(b)
			children := strings.NewReader(string(b))
			for children.Len() > 0 {
				subpackets = append(subpackets, readPacket(children))
			}
		} else {
			subpacketCount := readInt(reader, 11)
			for i := 0; i < subpacketCount; i++ {
				subpackets = append(subpackets, readPacket(reader))
			}

		}
		return Packet(operator{header{version, typeId}, subpackets})
	}
}

func (p literal) Checksum() int {
	return p.version
}

func (p operator) Checksum() int {
	checksum := p.version
	for _, sp := range p.subpackets {
		checksum += sp.Checksum()
	}
	return checksum
}

func (p literal) Evaluate() int {
	return p.value
}

func (p operator) Evaluate() int {
	switch p.typeId {
	case 0: // sum
		sum := 0
		for _, sp := range p.subpackets {
			sum += sp.Evaluate()
		}
		return sum
	case 1: // product
		product := 1
		for _, sp := range p.subpackets {
			product *= sp.Evaluate()
		}
		return product
	case 2: // minimum
		min := math.MaxInt
		for _, sp := range p.subpackets {
			if val := sp.Evaluate(); val < min {
				min = val
			}
		}
		return min
	case 3: // maximum
		max := 0
		for _, sp := range p.subpackets {
			if val := sp.Evaluate(); val > max {
				max = val
			}
		}
		return max
	case 5: // greater than
		if p.subpackets[0].Evaluate() > p.subpackets[1].Evaluate() {
			return 1
		} else {
			return 0
		}
	case 6: // less than
		if p.subpackets[0].Evaluate() < p.subpackets[1].Evaluate() {
			return 1
		} else {
			return 0
		}
	case 7: //equal to
		if p.subpackets[0].Evaluate() == p.subpackets[1].Evaluate() {
			return 1
		} else {
			return 0
		}
	default:
		panic("Invalid type id")
	}
}
