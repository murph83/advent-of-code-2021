package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
)

type cave string

var connections = make(map[cave][]cave)

func main() {
	readFile, _ := os.Open("../inputs/Day12.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		conn := strings.Split(fileScanner.Text(), "-")
		connections[cave(conn[0])] = append(connections[cave(conn[0])], cave(conn[1]))
		connections[cave(conn[1])] = append(connections[cave(conn[1])], cave(conn[0]))
	}

	part1()
	part2()
}

func part1() {

	var currentpath []cave
	var visited = make(map[cave]struct{})
	var paths [][]cave

	var dfs func(c cave)
	dfs = func(c cave) {
		if _, v := visited[c]; v {
			return
		}

		if c.isSmall() {
			visited[c] = struct{}{}
		}

		currentpath = append(currentpath, c)
		if cave("end") == c {
			completepath := make([]cave, len(currentpath))
			copy(completepath, currentpath)
			paths = append(paths, completepath)
		} else {
			for _, n := range connections[c] {
				dfs(n)
			}
		}
		currentpath = currentpath[:len(currentpath)-1]
		delete(visited, c)
	}

	dfs(cave("start"))
	fmt.Printf("Part 1: %d\n", len(paths))

}

func part2() {

	var currentpath []cave
	var visited = make(map[cave]int)
	var paths [][]cave

	var dfs func(c cave)
	dfs = func(c cave) {
		if visited[c] > 0 && (c == cave("start") || c == cave("end") || visitedAnyTwice(visited)) {
			return
		}

		if c.isSmall() {
			visited[c]++
		}

		currentpath = append(currentpath, c)
		if cave("end") == c {
			completepath := make([]cave, len(currentpath))
			copy(completepath, currentpath)
			paths = append(paths, completepath)
		} else {
			for _, n := range connections[c] {
				dfs(n)
			}
		}
		currentpath = currentpath[:len(currentpath)-1]
		if c.isSmall() {
			visited[c]--
		}
	}

	dfs(cave("start"))
	fmt.Printf("Part 2: %d\n", len(paths))

}

func visitedAnyTwice(visited map[cave]int) bool {
	for _, v := range visited {
		if v > 1 {
			return true
		}
	}
	return false
}

func (c *cave) isSmall() bool {
	for _, r := range *c {
		if !unicode.IsLower(r) && unicode.IsLetter(r) {
			return false
		}
	}
	return true
}
