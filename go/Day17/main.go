package main

import "fmt"

// puzzle input, no need to parse
// target area: x=236..262, y=-78..-58
var (
	xmin = 236
	xmax = 262
	ymin = -78
	ymax = -58
)

func main() {
	part1()
	part2()
}

func part1() {

	// the fastest you can go in the y direction is the speed that takes you from the starting height to the bottom of the range in one step (ymin)
	// the height is the sum from 1..n == n * n+1 /2
	fmt.Printf("Part 1: %d\n", ymin*(ymin+1)/2)
}

func part2() {
	onTargetCount := 0

	// test fire, using the velocities that overshoot the range as the limits.
	// there are probably tighter bounds that can be put on this (especially the min vx; 1 is clearly too low), but it's good enough.
	for vx := 1; vx <= xmax; vx++ {
		for vy := ymin; vy <= -ymin; vy++ {
			var x, y int
			onTarget := false
			for t := 0; x <= xmax && y >= ymin; t++ {
				dx := vx - t
				if dx < 0 {
					dx = 0
				}
				x += dx
				y += vy - t
				onTarget = onTarget || (x >= xmin && y <= ymax && x <= xmax && y >= ymin)
			}
			if onTarget {
				onTargetCount++
			}
		}
	}

	fmt.Printf("Part 2: %d\n", onTargetCount)
}
