package main

import (
	"bufio"
	"fmt"
	"os"
)

type point struct {
	x, y int
}

type line struct {
	start, end point
}

var lines []line

func main() {
	readFile, _ := os.Open("../inputs/Day05.input")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		var startx, starty, endx, endy int
		fmt.Sscanf(fileScanner.Text(), "%d,%d -> %d,%d", &startx, &starty, &endx, &endy)
		lines = append(lines, line{point{startx, starty}, point{endx, endy}})
	}

	part1()
	part2()
}

func part1() {
	multiplicities := make(map[point]int)
	for _, l := range lines {
		if l.isHorizontal() || l.isVertical() {
			for _, gp := range l.gridPoints() {
				add(multiplicities, gp)
			}
		}
	}

	count := 0
	for _, v := range multiplicities {
		if v >= 2 {
			count++
		}
	}

	fmt.Printf("Part1: %d\n", count)
}

func part2() {
	multiplicities := make(map[point]int)
	for _, l := range lines {
		for _, gp := range l.gridPoints() {
			add(multiplicities, gp)
		}
	}

	count := 0
	for _, v := range multiplicities {
		if v >= 2 {
			count++
		}
	}

	fmt.Printf("Part2: %d\n", count)
}

func add(m map[point]int, p point) {
	m[p] = m[p] + 1
}

func (l *line) isHorizontal() bool {
	return l.start.y == l.end.y
}

func (l *line) isVertical() bool {
	return l.start.x == l.end.x
}

func (l *line) contains(p point) bool {
	return (l.start.x == l.end.x && l.end.x == p.x) || ((l.end.y-l.start.y)*(p.x-l.end.x) == (p.y-l.end.y)*(l.end.x-l.start.x))
}

func (l *line) gridPoints() []point {
	var points []point
	for i := min(l.start.x, l.end.x); i <= max(l.start.x, l.end.x); i++ {
		for j := min(l.start.y, l.end.y); j <= max(l.start.y, l.end.y); j++ {
			p := point{i, j}
			if l.contains(p) {
				points = append(points, p)
			}
		}
	}
	return points
}

func min(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
