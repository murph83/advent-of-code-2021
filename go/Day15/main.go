package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/beefsack/go-astar"
)

type point struct {
	x, y int
}

var cave = make(map[point]int)
var xmax, ymax int

func main() {
	readFile, _ := os.Open("../inputs/Day15.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	i := 0
	for fileScanner.Scan() {
		for j, r := range fileScanner.Text() {
			cave[point{i, j}] = int(r - '0')
		}
		i++
	}
	xmax = i
	ymax = len(cave) / xmax

	part1()
	part2()
}

func part1() {
	_, d, _ := astar.Path(point{0, 0}, point{xmax - 1, ymax - 1})

	fmt.Printf("Part 1: %d\n", int(d))
}

func part2() {
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			if i > 0 || j > 0 {
				for x := 0; x < xmax; x++ {
					for y := 0; y < ymax; y++ {
						level := cave[point{x, y}] + i + j
						if level > 9 {
							level -= 9
						}
						cave[point{xmax*i + x, ymax*j + y}] = level
					}
				}
			}
		}
	}
	xmax = xmax * 5
	ymax = ymax * 5

	_, d, _ := astar.Path(point{0, 0}, point{xmax - 1, ymax - 1})

	fmt.Printf("Part 2: %d\n", int(d))
}

func (p point) PathNeighbors() []astar.Pather {
	var neighbors []astar.Pather
	for _, offset := range [][]int{
		{0, -1}, // up
		{-1, 0}, // left
		{1, 0},  // right
		{0, 1},  // down
	} {
		if _, n := cave[point{p.x + offset[0], p.y + offset[1]}]; n {
			neighbors = append(neighbors, point{p.x + offset[0], p.y + offset[1]})
		}
	}

	return neighbors
}

func (p point) PathNeighborCost(to astar.Pather) float64 {
	toP := to.(point)
	return float64(cave[toP])
}

func (p point) PathEstimatedCost(to astar.Pather) float64 {
	toP := to.(point)
	dx := toP.x - p.x
	if dx < 0 {
		dx = -dx
	}

	dy := toP.y - p.y
	if dy < 0 {
		dy = -dy
	}
	return float64(dx + dy)
}
