package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strings"
)

var template string
var insertionrules = make(map[string]string)

type memoKey struct {
	elements string
	depth    int
}

var frequencyMemo = make(map[memoKey]map[rune]int)

func main() {
	readFile, _ := os.Open("../inputs/Day14.input")
	//readFile, _ := os.Open("test")
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var inputs []string
	for fileScanner.Scan() {
		inputs = append(inputs, fileScanner.Text())
	}

	template = inputs[0]
	for i := 2; i < len(inputs); i++ {
		rule := strings.Split(inputs[i], " -> ")
		insertionrules[rule[0]] = rule[1]
	}

	part1()
	part2()

}

func part1() {
	max := 0
	min := math.MaxInt
	for _, f := range getFrequencies(template, 10) {
		if f < min {
			min = f
		}
		if f > max {
			max = f
		}
	}

	fmt.Printf("Part 1: %d\n", max-min)
}

func part2() {
	max := 0
	min := math.MaxInt
	for _, f := range getFrequencies(template, 40) {
		if f < min {
			min = f
		}
		if f > max {
			max = f
		}
	}

	fmt.Printf("Part 2: %d\n", max-min)
}

func getFrequencies(elements string, depth int) map[rune]int {
	if fm, q := frequencyMemo[memoKey{elements, depth}]; q {
		return fm
	}

	frequencies := make(map[rune]int)
	if depth == 0 {
		for _, r := range elements {
			frequencies[r]++
		}
	} else {
		nextStep := step(elements)
		// left half
		for r, f := range getFrequencies(nextStep[:len(elements)], depth-1) {
			frequencies[r] += f
		}
		// right half
		for r, f := range getFrequencies(nextStep[len(elements)-1:], depth-1) {
			frequencies[r] += f
		}
		// de-duplicate the center
		frequencies[rune(nextStep[len(elements)-1])]--
	}

	frequencyMemo[memoKey{elements, depth}] = frequencies
	return frequencies
}

func step(p string) string {
	pr := []rune(p)
	var sb, pair strings.Builder
	for i := 0; i < len(pr)-1; i++ {
		sb.WriteRune(pr[i])
		pair.Reset()
		pair.WriteRune(pr[i])
		pair.WriteRune(pr[i+1])
		sb.WriteString(insertionrules[pair.String()])
	}
	sb.WriteRune(pr[len(pr)-1])

	return sb.String()
}
