# Advent of Code 2021 - Clojure Edition
## Running
```shell
clj -M -m day01
```

## Solution index (deep link to the code)
|||
|-|-|
| Day  1 |  [src/day01.clj](src/day01.clj) |
| Day  2 |  [src/day02.clj](src/day02.clj) |
| Day  3 |  [src/day02.clj](src/day03.clj) |
| Day 17 |  [src/day17.clj](src/day17.clj) |
