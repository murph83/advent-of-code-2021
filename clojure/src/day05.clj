(ns day05
  "Advent of Code 2021 Day 5
  
  [https://adventofcode.com/2021/day/5](https://adventofcode.com/2021/day/5)"
  {:author "Murph <murph@opusdavei.org>"}
  (:require [clojure.java.io :as io]))

(defn colinear?
  [point line]
  (let [[x1 y1 x2 y2] line
        [x3 y3]       point]
    (or (= x1 x2 x3) 
        (= (* (- y2 y1) (- x3 x2)) 
           (* (- y3 y2) (- x2 x1))))))

(defn horizontal?
  [line]
  (let [[_ y1 _ y2] line] 
    (= y1 y2)))

(defn vertical?
  [line]
  (let [[x1 _ x2 _] line]
    (= x1 x2)))

(defn grid-points
  "Maps a line to the grid points it crosses. Lines are only ever slope -1, 0, 1, or infinite"
  [line]
  (let [[x1 y1 x2 y2] line]
   (cond
     (vertical? line) (map vector (repeat x1) (range (min y1 y2) (inc (max y1 y2))))
     (horizontal? line) (map vector (range (min x1 x2) (inc (max x1 x2))) (repeat y1))
     (pos? (/ (- x1 x2) (- y1 y2))) (map vector (range (min x1 x2) (inc (max x1 x2))) (range (min y1 y2) (inc (max y1 y2))))
     :else (map vector (range (min x1 x2) (inc (max x1 x2))) (range (max y1 y2) (dec (min y1 y2)) -1)))))

(defn parse-line
  [line]
  (map read-string (re-seq #"\d+" line)))

(defn -main [& _]
  (with-open [rdr (io/reader (io/resource "Day05.input"))]
    (let [lines (map parse-line (line-seq rdr))]
      (printf "Part 1 result: %d%n" (count (filter (fn [[_ v]] (> v 1)) (frequencies (mapcat grid-points (filter (some-fn horizontal? vertical?) lines))))))
      (printf "Part 2 result: %d%n" (count (filter (fn [[_ v]] (> v 1)) (frequencies (mapcat grid-points lines))))))))