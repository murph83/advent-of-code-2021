(ns day03
  "Advent of Code 2021 Day 3
  
  [https://adventofcode.com/2021/day/3](https://adventofcode.com/2021/day/3)"
  {:author "Murph <murph@opusdavei.org>"}
  (:require [clojure.java.io :as io]))

(defn digits 
  "Map a string to a sequence of digits"
  [n]
  (map #(Character/digit % 10) (str n)))

(defn most-common-bits
  "Calculate the most common bit in each position for a list of numbers"
  [report]
  (map #(Math/round (double (/ (reduce + %) (count %)))) (apply map vector report)))

(defn str->bin 
  "Convert a string to a binary integer"
  [n]
  (Integer/parseInt (apply str n) 2))

(defn part1 [report]
  (let [bits    (most-common-bits report)
        gamma   (str->bin (apply str bits))
        epsilon (str->bin (apply str (map {1 0 0 1} bits)))]
    (* gamma epsilon)))

(defn rating-search
  [report bit-criteria-fn]
  (loop [search  report
         bit-idx 0]
    (let [bit-criteria (nth (bit-criteria-fn search) bit-idx)
          matching (filter #(= bit-criteria (nth % bit-idx)) search)]
      (if (> (count matching) 1)
        (recur matching (inc bit-idx))
        (first matching)))))

(defn part2 [report]
  (let [oxygen-rating (str->bin (rating-search report most-common-bits))
        co2-rating    (str->bin (rating-search report #(map {1 0 0 1} (most-common-bits %))))]
    (* oxygen-rating co2-rating)))

(defn -main [& _]
  (with-open [rdr (io/reader (io/resource "Day03.input"))]
    (let [report (map digits (line-seq rdr))]
      (printf "Part 1 result: %d%n" (part1 report))
      (printf "Part 2 result: %s%n" (part2 report)))))
    