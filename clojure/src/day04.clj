(ns day04
  "Advent of Code 2021 Day 4
  
  [https://adventofcode.com/2021/day/4](https://adventofcode.com/2021/day/4)"
  {:author "Murph <murph@opusdavei.org>"}
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn mark-board
  [draw board]
  (map #(replace {draw -1} %) board))

(defn winning-board?
  [b]
  (some #(= -5 (apply + %)) (concat b (apply map vector b))))

(defn winning-boards
  "Create a sequence of winning board scores"
  [draws boards]
  (when-let [draw (first draws)]
    (let [{win true lose nil} (group-by winning-board? (map (partial mark-board draw) boards))]
      (lazy-seq (if (empty? win)
                  (winning-boards (rest draws) (concat win lose))
                  (concat (map #(* (apply + (replace {-1 0} (flatten %))) draw) win) (winning-boards (rest draws) lose)))))))

(defn -main [& _]
  (with-open [rdr (io/reader (io/resource "Day04.input"))]
    (let [input  (line-seq rdr)
          draws  (map read-string (str/split (first input) #","))
          boards (partition 5 (map #(map read-string (str/split (str/trim %) #"\s+")) (remove str/blank? (rest input))))
          wins   (take-while some? (winning-boards draws boards))]
      (printf "Part 1 result: %d%n" (first wins))
      (printf "Part 2 result: %d%n" (last wins)))))