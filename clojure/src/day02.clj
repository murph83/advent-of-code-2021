(ns day02
  "Advent of Code 2021 Day 2
  
  [https://adventofcode.com/2021/day/1](https://adventofcode.com/2021/day/1)"
  {:author "Murph <murph@opusdavei.org>"}
  (:require [clojure.java.io :as io]))

(defn part1 [commands]
  (let [{h :h
         d :d} (reduce
                (fn [r [_ direction units]]
                  (case direction
                    "forward" (update r :h #(+ % units))
                    "down" (update r :d #(+ % units))
                    "up" (update r :d #(- % units))))
                {:h 0 :d 0} 
                commands)]
    (* h d)))

(defn part2 [commands]
  (let [{h :h
         d :d} (reduce
                (fn [r [_ direction units]]
                  (case direction
                    "forward" (-> r (update :h #(+ % units)) (update :d #(+ % (* (:aim r) units))))
                    "down" (update r :aim #(+ % units))
                    "up" (update r :aim #(- % units))))
                {:h 0 :d 0 :aim 0} 
                commands)]
    (* h d)))

(defn -main [& _]
  (with-open [rdr (io/reader (io/resource "Day02.input"))]
    (let [commands (->> (line-seq rdr) (map #(re-matches #"(\D+) (\d+)" %)) (map #(update % 2 read-string)))]
      (printf "Part 1 result: %d%n" (part1 commands))
      (printf "Part 2 result: %d%n" (part2 commands)))))