(ns day06
  "Advent of Code 2021 Day 6
  
  [https://adventofcode.com/2021/day/6](https://adventofcode.com/2021/day/6)"
  {:author "Murph <murph@opusdavei.org>"}
  (:require [clojure.java.io :as io]))

(defn population
  ([fish] (population fish 0))
  ([fish day] (lazy-seq (cons (apply + fish) (population (update fish (mod (+ 7 day) 9) + (fish (mod day 9))) (inc day))))))

(defn parse-input
  [input]
  (let [fish-ages (map read-string (re-seq #"\d" input))]
    (reduce (fn [acc v] (update acc v inc)) '[0 0 0 0 0 0 0 0 0] fish-ages)))

(defn -main [& _]
  (with-open [rdr (io/reader (io/resource "Day06.input"))]
    (let [fish (parse-input (first (line-seq rdr)))]
      (printf "Part 1 result: %d%n" (nth (population fish) 80))
      (printf "Part 2 result: %d%n" (nth (population fish) 256)))))