(ns day17
  "Advent of Code 2021 Day 17
  
  [https://adventofcode.com/2021/day/17](https://adventofcode.com/2021/day/17)"
  {:author "Murph <murph@opusdavei.org>"})

(defn -main [& _]
  (let [[maxX minX maxY minY] '[262 236 -58 -78]
        shots-on-target       (distinct (for [vx     (range 1 (inc maxX))
                                              vy     (range minY (inc (- minY)))
                                              :let   [p (reductions (fn [[x y] t] [(+ x (max 0 (- vx t))) (+ y (- vy t))]) [0 0] (range))]
                                              t      (range)
                                              :let   [[x y] (nth p t)]
                                              :while (and (<= x maxX) (>= y minY))
                                              :when  (and (>= x minX) (<= y maxY))]
                                          [vx vy]))]
    (printf "Part 1 result: %d%n" (/ (* minY (inc minY)) 2))
    (printf "Part 2 result: %d%n" (count shots-on-target))))