(ns day01
  "Advent of Code 2021 Day 1
  
  [https://adventofcode.com/2021/day/1](https://adventofcode.com/2021/day/1)"
  {:author "Murph <murph@opusdavei.org>"}
  (:require [clojure.java.io :as io]))

(defn count-increasing
  "Takes a seq of vectors and counts the number where the first and last entry are monotonically increasing"
  [s]
  (count (filter #(< (first %) (last %)) s)))

(defn -main [& _]
  ; Read the input file into seq `depths`
  (with-open [rdr (io/reader (io/resource "Day01.input"))]
    (let [depths (map read-string (line-seq rdr))]
      
      (printf "Part 1 result: %d%n" (count-increasing (partition 2 1 depths)))
      (printf "Part 2 result: %d%n" (count-increasing (partition 4 1 depths))))))
