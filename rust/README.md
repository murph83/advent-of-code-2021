# Advent of Code 2021 - Rust Edition
## Running
```shell
cargo run --bin day01
```

## Solution index (deep link to the code)
|||
|-|-|
| Day 1 |  [src/bin/day01.rs](src/bin/day01.rs) |
| Day 2 |  [src/bin/day02.rs](src/bin/day02.rs) |
