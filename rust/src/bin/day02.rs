/// Advent of Code 2021 Day 2
/// [https://adventofcode.com/2021/day/2](https://adventofcode.com/2021/day/2)
fn main() {
    let input = include_str!("Day02.input");

    let mut commands = Vec::new();
    for line in input.lines() {
        commands.push(parse_command(&line));
    }
    
    part1(&commands);
    part2(&commands);
}

enum Direction {
    Forward, Down, Up
}

struct Command {
    direction: Direction,
    units: i32
}

fn parse_command(line: &str) -> Command {
    let mut c = line.split_whitespace();
    
    let direction = match c.next() {
        Some("forward") => Direction::Forward,
        Some("down")    => Direction::Down,
        Some("up")      => Direction::Up,
        _ => panic!(),
    };

    let units = c.next().unwrap().parse().unwrap();
    
    Command {direction, units}
}

fn part1(commands: &Vec<Command>) {
    // Initial position
    let mut h = 0;
    let mut d = 0;

    for command in commands {
        match command.direction {
            Direction::Forward => h += command.units,
            Direction::Down    => d += command.units,
            Direction::Up      => d -= command.units,
        }
    }

    println!("Part 1 result {}", h * d);
}

fn part2(commands: &Vec<Command>) {
    // Initial position
    let mut h = 0;
    let mut d = 0;
    let mut aim = 0;

    for command in commands {
        match command.direction {
            Direction::Forward => {
                h += command.units;
                d += command.units * aim;
            },
            Direction::Down    => aim += command.units,
            Direction::Up      => aim -= command.units,
        }
    }


    println!("Part 2 result {}", h * d);
}
    