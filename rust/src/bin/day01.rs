/// Advent of Code 2021 Day 1
/// [https://adventofcode.com/2021/day/1](https://adventofcode.com/2021/day/1)
fn main() {
    let input = include_str!("Day01.input");
    let depths: Vec<i16> = input.lines().map(|s| s.parse().unwrap()).collect();

    let count_increasing = |window| {
        let mut count = 0;
        for i in window..depths.len() {
            if depths[i] > depths[i-window] {
                count+=1;
            }
        }
        return count;    
    };
    
    println!("Part 1 result: {}", count_increasing(1));
    println!("Part 2 result: {}", count_increasing(3));
}
