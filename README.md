# Advent of Code 2021
This repository contains solutions to the AoC 2021 problems. This year, I'm using the exercises as an excuse to learn a few things.

|Folder|Language|Learning Goals|
|------|--------|--------------|
| [java](java) | Java | A nicely-documented pure-Java implementation. JBang scaffolding and templates|
| [rust](rust) | Rust | Very new to Rust, thought I might give it a try |
| [clojure](clojure) | Clojure | I love Clojure. JVM and Lisp together forever |

|    |1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|
|----|-|-|-|-|-|-|-|-|-|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
|java|[ :star: :star: ](java/Day01.java)|[ :star: :star: ](java/Day02.java)|[ :star: :star: ](java/Day03.java)|[ :star: :star: ](java/Day04.java)|[ :star: :star: ](java/Day05.java)|[ :star: :star: ](java/Day06.java)|[ :star: :star: ](java/Day07.java)|[ :star: :star: ](java/Day08.java)|[ :star: :star: ](java/Day09.java)|[ :star: :star: ](java/Day10.java)|[ :star: :star: ](java/Day11.java)|[ :star: :star: ](java/Day12.java)|[ :star: :star: ](java/Day13.java)|[ :star: :star: ](java/Day14.java)|[ :star: :star: ](java/Day15.java)|[ :star: :star: ](java/Day16.java)|[ :star: :star: ](java/Day17.java)|[ :star: :star: ](java/Day18.java)|[ :star: :star: ](java/Day19.java)|[ :star: :star: ](java/Day20.java)|[ :star: :star: ](java/Day21.java)|
